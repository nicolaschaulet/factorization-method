  function [ErrVol,ErrCenter]=plotIND3d(datafile, filesave,eps, x1, x2, x3)
% Usage: [ErrVol,ErrCenter]=plotIND3d(datafile, filesave,eps, x1, x2, x3)
%
% General:
% Plots an isosurface of a three dimensional function and compare the
% result with the expected one
%
% Input: 
% + datafile - filename of the file containing the data
%        line i -  x  y  z  F(x,y,z) 
% + filesave - filename for the recording
% + eps - numerical value for the isosurface we plot ( 0 < eps < 1)
% Optional: x1, x2, x3 \in {'x','-x','y','-y','z','-z'} -> axis' labels
%
% Output:
% + ErrVol - error on the reconstructed volume
% + ErrCenter - error on the reconstructed center
%------------------------------------------------------------------------------------------------------------------------

% Change this to plot the exact shape
plottrue=1;

% Location of the defect
ss=strfind(datafile,'_1');
if (~isempty(ss))
    location=1;
else
    location=2;
end;

% Variable to detect the kind of object we inspect
% 1 -> rat
% 2 -> head with skull
% 3 -> head without skull
% 4 -> Cylinder
geotype=0; 

% ----------------------------------------------
% Load the geometry of the inspected object from saved meshes
% Rat (semi spherical)
ss=strfind(filesave,'rat');
if (~isempty(ss))
    geotype=1;
    %load '/Users/nicolas/Recherche/Codes/EIT/3DFullImpedanceModel/Mesh/rat/BETTER_Mesh_tank_100k_mat_ref.mat';
    %load '/Users/nicolas/Recherche/Codes/EIT/FwdSolverAndFactorization/MeshesAndCo/cylinder_flat_array.mat'
    srf=0;%dubs3_2(Mesh.Tetra);
    vtx=0;%Mesh.Nodes;
    tri=0;%Mesh.Tetra;
end

% Head shape
ss=strfind(filesave,'head');
if (~isempty(ss))
    ss=strfind(filesave,'skull');
    if (~isempty(ss)) % With Skull
        geotype=2;
        load '/Users/nicolas/Recherche/Codes/EIT/3DFullImpedanceModel/Mesh/Head_Tank_with_skull_meshes/mesh_headtank_with_skull_31elec_5mm.mat'
        %vtx1=vtx;
        %vtx(:,2)=-vtx1(:,3);
        %vtx(:,3)=vtx1(:,2);
        vtx(:,2) = -vtx(:,2) +100;
        vtx(:,1)=vtx(:,1)+75;
        vtx(:,3)=vtx(:,3)+75;
    else % without skull
        load '/Users/nicolas/Recherche/Codes/EIT/3DFullImpedanceModel/Mesh/Head_Tank_without_skull_meshes/01_New_meshes/Meshes_without_skull/Nelec_31/mesh_headtank_elec_31_coarse1.mat'
        geotype=3;
    end
    srf=dubs3_2(tri);
end

% Cylinders
ss=strfind(filesave,'cyl');
if (~isempty(ss))
    mesh=loadmesh('../MeshesAndCo/SA060.mesh');
    vtx = mesh.vtx(:,1:3);
    srf = mesh.boundary(:,1:3);
    geotype=4;
end



%------ Loads the data and the plot grid
filename=[datafile,'.vtk'];
[point ind]=readVTK(filename);

% A=dlmread(filename);
% point = A(:,1:3);
% ind = A(:,4);
% clear A;

 if (geotype==2)
     %point1=point;
     %point(:,2)=-point1(:,3);
     %point(:,3)=point1(:,2);
      point(:,2)=-point(:,2)+100;
      point(:,1)=-(point(:,1)+75);
      point(:,3)=point(:,3)+75;
 end

%------ Interpolates the data on a regular grid
% Property of the grid
xmin=min(point);
xmax=max(point);
dx=abs((xmin-xmax)/40);

% Build teh grid
x_edge=[min(point(:,1)):dx(1):max(point(:,1))];
y_edge=[min(point(:,2)):dx(2):max(point(:,2))];
z_edge=[min(point(:,3)):dx(3):max(point(:,3))];
[X,Y,Z]=meshgrid(x_edge,y_edge,z_edge); 

% Interpolation
F= TriScatteredInterp(point(:,1),point(:,2),point(:,3),ind(:));
Zex=F(X,Y,Z);

% normalization of F between 0 and 1
vmax = max(max(max(Zex)));
vmin = min(min(min(Zex)));
Zex=(Zex-vmin)/(vmax-vmin);

% Quantitative estimates
%object=find(Zex>=eps);
%object1=object(find(X(object)>-0.045));
%Vol=size(object1,1)*dx(1)*dx(2)*dx(3)
%center=[mean(X(object1)),mean(Y(object1)),mean(Z(object1))]

%------ Computation of quantitative errors
% Computes the volume and centers of the detected object
if (geotype==4)&&(location==2) % two distinct obstacles
    object=find(Zex>=eps);
    object1=object(find(Y(object)<1));
    object2=object(find(Y(object)>1));
    Vol=[size(object1,1);size(object2,1)]*dx(1)*dx(2)*dx(3);
    center=[mean(X(object1)),mean(Y(object1)),mean(Z(object1));
        mean(X(object2)),mean(Y(object2)),mean(Z(object2))];
else % one single obstacle
    object=find(Zex>=eps);
    Vol=size(object,1)*dx(1)*dx(2)*dx(3);
    center=[mean(X(object)),mean(Y(object)),mean(Z(object))];
end
% Compares with exact ones (when known)
switch geotype
    case 1
        ErrVol=[abs(Vol(1)-0.002^3)/(0.002^3);0];
        ErrCenter=[norm(abs(center(1,:)-[-0.005,0,0.006]));0]./max(xmax); 
    case 4
        if location==2
            ErrVol=[abs(Vol(1)-4/3*pi)/(4/3*pi);abs(Vol(2)-4/3*pi)/(4/3*pi)];
            ErrCenter=[norm(abs(center(1,:)-[5,-2,2]));norm(abs(center(2,:)-[0,5,2]))]./max(xmax);
        else
            ErrVol=[abs(Vol(1)-4/3*pi)/(4/3*pi);0];
            ErrCenter=[norm(abs(center(1,:)-[0,5,2]))/max(xmax);0];
        end
    case {2,3}
        ErrVol=[abs(Vol(1)-4/3*pi*10^3)/(4/3*pi*10^3);0];
        if location==1
            ErrCenter=[norm(abs(center(1,:)-[0,0,0]))/max(xmax);0];
        else
            ErrCenter=[norm(abs(center(1,:)-[40,40,0]))/max(xmax);0];
        end
    otherwise
        disp('Object not defined for this geometry!!')
end
      
        

%------ Plot initialization
gcf=figure;
p = patch('Vertices', vtx, 'Faces', srf); % plots the inspected object
set(p, 'FaceColor', 'black', 'EdgeColor', 'none'); 
alpha(0.1);
hold on

if nargin < 5 % default values for axis labels
x1 = 'X' ; x2 = 'Y' ; x3 = 'Z';
end;	 
if eps > 1
disp([' '])
disp(['The third argument has to be < 1'])
disp([' '])
error('........................... Stopped')
end;


%------ Plots the isosurface
comm = sprintf('[F,V]=isosurface(%s,%s,%s,Zex, eps);',x1,x2,x3);
eval(comm);
p = patch('Vertices', V, 'Faces', F);

% Aspect of the plot
set(p, 'FaceColor', 'red', 'EdgeColor', 'none');
daspect([1 1 1]) ;
view(3) ;
camlight; 
lighting phong;
set(gca, 'FontSize',22);
set(gca, 'LineWidth', 1.5);
grid on
      
% The axis
comm = sprintf('xmax = max(max(max(%s)));',x1);
eval(comm);
comm = sprintf('xmin = min(min(min(%s)));',x1);
eval(comm);
comm = sprintf('ymax = max(max(max(%s)));',x2);
eval(comm);
comm = sprintf('ymin = min(min(min(%s)));',x2);
eval(comm);
comm = sprintf('zmax = max(max(max(%s)));',x3);
eval(comm);
comm = sprintf('zmin = min(min(min(%s)));',x3);
eval(comm);
axis([xmin xmax ymin ymax zmin zmax])

% We put the projections
hold on
XV=V(:,1);YV=V(:,2);ZV=V(:,3);
clear V

VXZ(:,1)=XV;VXZ(:,2)=ymax*ones(size(YV)); VXZ(:,3)=ZV;
patch('Vertices', VXZ, 'Faces', F, 'FaceVertexCData', [1. 1. 1.], ...
'FaceColor', 'flat','EdgeColor', 'none');

VXZ(:,1)=xmax*ones(size(XV)); VXZ(:,2)=YV; VXZ(:,3)=ZV;
patch('Vertices', VXZ, 'Faces', F, 'FaceVertexCData', [1. 1. 1.], ...
'FaceColor', 'flat','EdgeColor', 'none');

VXZ(:,1)=XV; VXZ(:,2)=YV; VXZ(:,3)=zmin*ones(size(ZV));
patch('Vertices', VXZ, 'Faces', F, 'FaceVertexCData', [1. 1. 1.], ...
'FaceColor', 'flat','EdgeColor', 'none');
clear VXZ  V F

drawnow 
xlabel('x');ylabel('y');zlabel('z');

%------ Plots the true object for comparison
if plottrue
theta=[0:0.01:2*pi];
switch geotype
    case 1 % Rat
        R=0.002;
        theta=[-R:R/50:R];
        X=ones(4*size(theta))*xmax;X=X(1,:);
        Y=[theta,R*ones(size(theta)),-theta,-R*ones(size(theta))];
        Z=[-0.006+R*ones(size(theta)),-0.006-theta,-0.006-R*ones(size(theta)),-0.006+theta];
        YY=ones(4*size(theta))*ymax;Y=[Y;YY(1,:)];
        X=[X;-0.005+[theta,R*ones(size(theta)),-theta,-R*ones(size(theta))]];
        Z=[Z;[-0.006+R*ones(size(theta)),-0.006-theta,-0.006-R*ones(size(theta)),-0.006+theta]];
        ZZ=ones(4*size(theta))*zmin;Z=[Z;ZZ(1,:)];
        X=[X;-0.005+[theta,R*ones(size(theta)),-theta,-R*ones(size(theta))]];
        Y=[Y;[R*ones(size(theta)),-theta,-R*ones(size(theta)),+theta]];
      case {2,3} % Head (with and without skull)
        R=10;
        switch location
            case 1 % middle
                X=theta; X(:)=xmax;Y=R*cos(theta);Z=R*sin(theta);
                X=[X;R*cos(theta)];Y=[Y;ymax*ones(size(theta))];Z=[Z;R*sin(theta)];
                X=[X;R*cos(theta)]; Y=[Y;R*sin(theta)];Z=[Z;zmin*ones(size(theta))];
            case 2 % back side
                X=theta; X(:)=xmax; Y=40+R*cos(theta);Z=R*sin(theta);
                X=[X;40+R*cos(theta)];Y=[Y;ymax*ones(size(theta))];Z=[Z;R*sin(theta)];
                X=[X;40+R*cos(theta)];Y=[Y;40+R*sin(theta)];Z=[Z;zmin*ones(size(theta))];
        end
    case 4 % cylinder
        R=1;
        switch location
            case 1
                X=theta; X(:)=xmax;Y=5+R*cos(theta);Z=2+R*sin(theta);
                X=[X;R*cos(theta)];Y=[Y;ymax*ones(size(theta))];Z=[Z;2+R*sin(theta)];
                X=[X;R*cos(theta)]; Y=[Y;5+R*sin(theta)];Z=[Z;zmin*ones(size(theta))];
            case 2
                X=theta; X(:)=xmax;Y=5+R*cos(theta);Z=2+R*sin(theta);
                X=[X;R*cos(theta)];Y=[Y;ymax*ones(size(theta))];Z=[Z;2+R*sin(theta)];
                X=[X;R*cos(theta)]; Y=[Y;5+R*sin(theta)];Z=[Z;zmin*ones(size(theta))];
                X=[X;xmax*ones(size(theta))];Y=[Y;-2+R*cos(theta)];Z=[Z;2+R*sin(theta)];
                X=[X;5+R*cos(theta)];Y=[Y;ymax*ones(size(theta))];Z=[Z;2+R*sin(theta)];
                X=[X;5+R*cos(theta)]; Y=[Y;-2+R*sin(theta)];Z=[Z;zmin*ones(size(theta))];
        end
    otherwise
        disp('Geometry not defined!!');
end
nbplot=size(X,1);
for i=1:nbplot
    plot3(X(i,:),Y(i,:),Z(i,:),'g-','Linewidth',2.5);
end
end
        

% Saves the plot
saveperso(gcf,filesave);
end
%%  
  

% ----------------------------------- %
%   Auxilary Functions
% ----------------------------------- %

%% To save a plot
function [] =saveperso(axiss,filepdf)
% Usage: [] = saveperso(axiss,filepdf);
%
% General:
% Saves a plot
%
% Input: 
% + axiss - reference to figure to save
% + filepdf - filename
%------------------------------------------------------------------------------------------------------------------------

  set(axiss, 'PaperPosition', [0 0 5 5]);
  set(axiss,'papersize',[5 5]);
  saveas(axiss,[filepdf,'.pdf']);
end 
%%

%% To read point data from a simple vtk files
function [point ind] =readVTK(filename)
    fid = fopen(filename);
    line = fgetl(fid);
    while ischar(line)
        ss=strfind(line,'POINTS');
        if (~isempty(ss))
            nbP = sscanf(line,'POINTS %d FLOAT');
            point = fscanf(fid,'%f %f %f',[3 nbP]);
            point=point';
        end 
        ss=strfind(line,'CELLS');
        if (~isempty(ss))
            [nbC] = sscanf(line,'CELLS %d %d');
            fscanf(fid,'%d %d %d %d %d',[5 nbC(1)]);
            clear b,nbC;
        end 
        ss=strfind(line,'CELL_TYPES');
        if (~isempty(ss))
            [nbC] = sscanf(line,'CELL_TYPES %d');
            fscanf(fid,'%f',[1 nbC]);
            clear b,nbC;
        end 
        ss=strfind(line,'POINT_DATA');
        if (~isempty(ss))
            nbD = sscanf(line,'POINT_DATA %d');
            line = fgetl(fid); line = fgetl(fid);
            ind = fscanf(fid,'%f',[1 nbD]);
            ind=ind';
        end 
        line = fgetl(fid);
    end
end
    
%%
function [srf] = dubs3_2(tri)
% Usage: [srf] = dubs3_2(tri);
%
% General:
% Auxiliary function that extract the boundary faces of a given 3D volume
%
% Input: 
% + tri - simplices matrix {k x 4}
%
% Output:
% + srf - outer boundary surfaces {no. of external surfaces x 3}
%------------------------------------------------------------------------------------------------------------------------

% create a list of all element facetts & sort
S = [tri(:,[1 2 3]); tri(:,[1 2 4]); tri(:,[2 3 4]); tri(:,[1 3 4])];
clear tri
N = sort(S,2);
clear S;
M = sortrows(N);
clear N;

i = 1;
inc = 1;

while i < size(M,1);
    ithrow = M(i,:);
    jthrow = M(i+1,:); 
    if ithrow == jthrow 
        i = i + 2;
    else
        % put in boundary node list
        srf(inc,:) = M(i,:);
        inc = inc + 1;
        i = i + 1;
    end
end
end
%%

%%


