%% Script to analyse the influence of putting zeros on the DtN map
close all;
clear all;
Path = '/Users/nicolas/Recherche/Codes/EIT/FwdSolverAndFactorization/3DFullImpedanceModel/Data/';

bgfile = [Path 'head_skull_31_rougth_1_20.bg'];
inclfile =[Path 'head_skull_31_rougth_1_20.incl'];
prtfile =[Path 'eeg31_20.prt'];

BG = read_ND_ff(bgfile);
INCL = read_ND_ff(inclfile);

[U,S,V] = svd(BG-INCL);


BGz = put_zeros(bgfile,prtfile);
INCLz = put_zeros(inclfile,prtfile);

write_ND_ff([bgfile(1:end-3) '_noinjecting.bg'], BGz);
write_ND_ff([inclfile(1:end-5) '_noinjecting.incl'], INCLz);

[Uz,Sz,Vz] = svd(BGz-INCLz);
figure
plot([abs(diag(S)-diag(Sz))./diag(S)]);
