function [] = write_ND_ff(file,ND);
    [I J C ] = find(ND);
    fid = fopen(file,'w');
    fprintf(fid,'%i\n',size(I,1));
    fprintf(fid,'%i %i %f\n',[I-1 J-1 C]');
end