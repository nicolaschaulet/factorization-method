function [B] = put_zeros(DtNfile,prtfile)
    B = read_ND_FF(DtNfile);
    
    prt = dlmread(prtfile);
    if size(prt,2)>2
        prt = prt(:,2:3);
    end
    mask =zeros(size(B));
    for j=1:size(prt,1)
        B(prt(j,1),j) =0;
        B(prt(j,2),j) =0;
        mask(prt(j,1),j) =1;
        mask(prt(j,2),j) =1;
    end
    figure
    imagesc(mask);
    

end