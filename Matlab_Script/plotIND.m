%% To plot the result of the factorization on a non regular grid
%close all
clear all
nomfichex='Test/cylinder.txt';
%filefreq='RealData/Carrot_Emma_08032013/background_carrot.mat';%'RealData/Potatoe_Tubga_06032013/freq_30.mat';%
%load(filefreq);
freqmax=1;%size(freq_list,2);
axiss=figure;
indtab=[];
for i=1:freqmax
%fidex=fopen([nomfichex,num2str(i-1),'.txt'],'r');
fidex=fopen(nomfichex,'r');
ind=[];
point=[];
while (feof(fidex)==0)
   pointloc=fscanf(fidex,'%f',2);
   indloc=fscanf(fidex,'%f',1);
   point = [point;pointloc'];
   ind = [ind indloc] ;   
end
fclose(fidex);

dx=0.001;
x_edge=[min(point(:,1)):dx:max(point(:,1))];
y_edge=[min(point(:,2)):dx:max(point(:,2))];
[X,Y]=meshgrid(x_edge,y_edge); 
ind = (ind - min(ind))./(max(ind)-min(ind));
Zex=griddata(point(:,1),point(:,2),ind(:),X,Y);
indtab=[indtab,ind'];
%Zex=(Zex-min(ind)*ones(size(Zex)))/(max(ind)-min(ind));

%subplot(4,ceil(freqmax/4),i);
%title(['frequency=',num2str(freq_list(i)),'Hz']);
figure;
colormap('Hot');
value=.9;
hold on
Zex(Zex<value)=0;
surf(X,Y,Zex,'EdgeColor','none');
view(2);


end
% figure;
% indv= mean(indtab,2);
% Zex=griddata(point(:,1),point(:,2),indv(:),X,Y);
% colormap('Hot');
% value=.9;
% surf(X,Y,Zex,'EdgeColor','none');
% view(2);
