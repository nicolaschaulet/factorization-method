////////////////////////////////////////////////////////////////////////
// Matrix operations
//    N. Chaulet

#ifndef matrix_hpp
#define matrix_hpp

#include "main.hpp"

using namespace arma;
using namespace std;

// reads the DtN in some basis
mat readDtN(string &); 

// build the passga matrix from euclidian basis to fourier basis
mat euclFourier(const int & ,const int & );

// put relative noise in M, noiselevel is in % in spectral norm
void put_noise(mat & M,const double & noiselevel);

// build a matrix to convert an absolute potential map to a difference one
mat abs_to_dif(const int & n);

#endif /* matrixpart.hpp */
