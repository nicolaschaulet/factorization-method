%% Small Script to convert a mat file to a FreeFem++ mesh

clear
load('/mnt/keg/Nicolas/Meshes/Mesh_RedNeoNate_no_skull_2014.mat')
fid=fopen('/mnt/keg/Nicolas/factorization-method/MeshesAndCo/Mesh_RedNeoNate_no_skull.mesh','w');
fprintf(fid,'MeshVersionFormatted 1\n\nDimension 3\n\nVertices\n');
fprintf(fid,'%d\n',size(Mesh.Nodes,1));
vtx = [Mesh.Nodes zeros(size(Mesh.Nodes,1),1)];
fprintf(fid,'%f %f %f %d\n',vtx');
fprintf(fid,'\nTetrahedra\n%d\n',size(Mesh.Tetra,1));
tri = [Mesh.Tetra zeros(size(Mesh.Tetra,1),1)];
fprintf(fid,'%d %d %d %d %d\n',tri');
fclose(fid);