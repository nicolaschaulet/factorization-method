////////////////////////////////////////////////////////////
// Data reading for the factorization method
// N. Chaulet

#include "data.hpp"


void read_data(const string & file,vector<string> &filesin,double * param)
{
  ifstream fin; fin.open(file.c_str());
  if (!fin){
    cout<<"Problem to open file "<<file<<endl;
    exit(0);
  }
    
  string s,f;
  while (!fin.eof()) {
    getline(fin,s);
    if (s.find("kill")!=string::npos) {
      fin>>param[0];}
    if (s.find("polarization")!=string::npos) {
      fin>>param[1]; }
    if (s.find("noise")!=string::npos) {
      fin>>param[2]; }
    if (s.find("Difference between two electrodes")!=string::npos) {
      fin>>param[3]; }
    if (s.find("Spectral")!=string::npos) {
      fin>>param[4]; }
    if (s.find("Analytic Green")!=string::npos) {
      fin>>param[5]; }
    if (s.find("Dimension")!=string::npos) {
      fin>>param[6]; }
    if (s.find("Difference map or two NtD maps")!=string::npos) {
      fin>>param[7]; }
    if (s.find("Grid")!=string::npos) {
      fin>>f; filesin.push_back(f); }
    if (s.find("Electrodes")!=string::npos) {
      fin>>f; filesin.push_back(f); }
    if (s.find("Background")!=string::npos) {
      fin>>f; filesin.push_back(f); }
    if (s.find("Inclusion")!=string::npos) {
      fin>>f; filesin.push_back(f); }
    if (s.find("Plot")!=string::npos) {
      fin>>f; filesin.push_back(f); }
  }
  fin.close();
  return;
}
