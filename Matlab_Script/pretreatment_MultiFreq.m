function []= pretreatment_EIT()
root='Carrot_Emma_08032013/';
freqmax=27;
protocol='polar32';

Iin=133*10^-6;

% load of the protocol matrix
% Careful, this is only for plor32 or polar16 only
if (protocol=='polar32')
    load(['Potatoe_Tubga_06032013/electrode_protocol/pol_32.prt']);
    In=pol_32(:,1:2);
    OUT=pol_32(:,3:4);
    nbcurrent=16; nbelec=32;
elseif (protocol=='polar16')
    load(['Camille/KHU/Pol_16_protocol.txt']);
    nbcurrent=8; nbelec=16;
    In=Pol_16_protocol(:,1:2);
    OUT=Pol_16_protocol(:,3:4);
else
    exit(0);
end

proto=zeros(nbcurrent*nbelec,nbelec);
proto_m=zeros(nbcurrent*nbelec,nbcurrent*(nbelec-4));
    
j=2;

for (i=0:(nbcurrent-1))
    count=1;
    ct=1;
    if (i>0)
        ct=0;
        j=1;
    end
    while count<(nbelec-3)
        igl=i*(nbelec-4)+count;
        jgl=i*nbelec+j;
        if (In(igl,1)==j+1)||(In(igl,2)==j+1)
            j=j+2;
            ct=ct+1;
        else
            if (i>1)
                proto_m(jgl,i*(nbelec-4)+rem((nbelec-4)+count-(i),nbelec-4)+1)=1;
            else
                proto_m(jgl,i*(nbelec-4)+count)=1;
            end
            proto(jgl,j)=-1;
            proto(jgl,rem(j,nbelec)+1)=1;
            j=j+1;
            count=count+1;
        end
    end
end



for freq=1:freqmax

% load of the background data
load([root,'background_carrot.mat']);

% Average over frames
data_av(:,:)=mean(data,3);
bg=proto_m*real(data_av(freq,:))';
test=real(data_av(freq,:))';


% load of the background data
load([root,'perturbation_carrot2.mat']);

% Average over frames
freq_meas=5;
data_av(:,:)=mean(data,3);
load([root,'carrot.mat']);
incl=proto_m*real(carrot_trunc(freq)*data_av(freq,:))';
%incl=proto_m*real(data_av(freq,:))';
bg=proto_m*real(carrot_trunc(freq_meas)*data_av(freq_meas,:))';
%bg=proto_m*real(data_av(freq_meas,:))';
%incl=proto_m*real(data_av(freq,:))';
alpha=bg'*incl/(bg'*bg);
%bg=alpha*bg;

% for (i=1:7)
% incl((i-1)*16+i+1)=0;
% incl((i-1)*16+i+9)=0;
% bg((i-1)*16+i+1)=0;
% bg((i-1)*16+i+9)=0;
% end
% incl(7*16+9)=0;
% incl(7*16+1)=0;
% bg(7*16+9)=0;
% bg(7*16+1)=0;
% 
% for (i=1:8)
% figure
% plot(incl(((i-1)*16+1):i*16)-bg(((i-1)*16+1):i*16));
% end
    


[I,J,D]=vectomat2(bg,nbelec,nbcurrent);
fid = fopen([root,'IN/bg_',num2str(freq-1),'.txt'],'w');
fprintf(fid,'%d\n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
[I,J,D]=vectomat2(incl,nbelec,nbcurrent);
fid = fopen([root,'IN/incl_',num2str(freq-1),'.txt'],'w');
fprintf(fid,'%d\n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
end
end

function [I,J,D]= vectomat2(vec,nbelec,nbcurrent)
I=zeros(size(vec,1),1);
J=I;
D=vec(:,1);
count=1;
for j=1:nbcurrent
    for i=1:nbelec
        I(count)=i;
        J(count)=j;
        count=count+1;
    end
end
end


% Difference NtD map
% incl=proto_m*real(carrot_trunc(7)*data_av(7,:))';
% bg=proto_m*real(carrot_trunc(14)*data_av(14,:))';
% [I,J,D]=vectomat(-bg+incl);
% Aexp=full(spconvert([I,J,D]));


% 
% Transformation of synthetic data to make them as the experimental ones
% 
%  load '/Users/nicolas/Recherche/FreeFem/codes/EIT/2DFullImpedanceModel/OUT/resistivitybg.txt'
%  load '/Users/nicolas/Recherche/FreeFem/codes/EIT/2DFullImpedanceModel/OUT/resistivityincl.txt'
%  resistivitybg(:,1:2)=resistivitybg(:,1:2)+1;
%  resistivityincl(:,1:2)=resistivityincl(:,1:2)+1;
%  A=full(spconvert(resistivitybg)-spconvert(resistivityincl));
%  for (i=1:16)
%     Aout=proto(1+(i-1)*32:(i)*32,:)*A(:,i);
%     A(:,i)=Aout;
% end
% [I,J,D]=vectomat(A);
% fid = fopen('testreal.txt','w');
% fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
% fclose(fid);
% 
% A=A/norm(A)*norm(Aexp);
% % Various plots
% for i=1:16
%     figure
%     plot(A(:,i));
%     hold on
%     plot(Aexp(:,i),'r');
% end





