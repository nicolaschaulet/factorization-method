clear
close all

%felec='/Users/nicolas/Recherche/Codes/EIT/3DFullImpedanceModel/Mesh/rat/rat_128.mat'; % This mesh is in m
fmesh='../Meshes/SA060.mat';
load(fmesh);
Mesh.vtx=vtx; Mesh.tri=tri;%optimize7_d(vtx,tri,true);
Mesh.srf = dubs3_2(Mesh.tri);
%poss=load(felec);
%pos=electrodes;
% region=zeros(size(Mesh.tri,1),1);
% region(find(sigma==0.44))=1;
% region(find(sigma==1.79))=2;
% region(find(sigma==0.018))=3;
% region(find(sigma==0.3))=4;
pos=0.01*pos;
Mesh.vtx = 0.01*Mesh.vtx;
[Mesh.elec, Mesh.cnts, Mesh.surf_area] = set_electrodes_var...
    (Mesh.vtx, Mesh.srf,pos,0.01,'w',false);
gmsh_exporter(Mesh.vtx,Mesh.tri,mat_ref,Mesh.elec,...
    'MeshesAndCo/SAO60',pos);
%gmsh_exporter(0.95*Mesh.vtx,Mesh.tri,zeros(size(Mesh.tri,1),1),Mesh.elec,...
%     '../../Data/rat_experiment_128grid');
printelec(Mesh,pos);
% patchboundary(Mesh.vtx,Mesh.tri,pos,zeros(size(Mesh.tri,1),1),...
%    '../../Data/rat_experiment')