////////////////////////////////////////////////////////////////////////
//  Main Factorisation
//    N. Chaulet
//


#include "geometrie.hpp"
#include "main.hpp"
#include "matrixpart.hpp"
#include "algos.hpp"
#include "data.hpp"

using namespace arma;
using namespace std;

#define PI datum::pi


int main(int argc, char** argv)
{
  // Load the data
  string data_file="data_factorization.dat";
  double param[8];
  vector<string> vs(0);
  read_data(data_file,vs,param);
  bool artificialless=bool(param[0]);
  int nbpol=int(param[1]);
  double delta=param[2];
  bool difference=bool(param[3]);
  int spec_trunc=int(param[4]);
  int analyticgreen=int(param[5]);
  double dimension=param[6];
  bool diffmap=param[7];
  string filegrid=vs[0];
  string fileelec=vs[1];
  string fileDtNbg=vs[2];
  string fileDtNincl=vs[3];
  string plot_file=vs[4];

  // Iinitialization of geometric tables
  vector<point> electrodes;
  vector<point> grid;
  vector<vector<point> > G;
  vector<vector<int> > tetra;

  // Read electrodes positions and inversion grid
  readpoints(fileelec,electrodes);
  if (dimension==2) // in dimension 2, just a list of points
    readpoints(filegrid,grid);
  else // in dimension 3, a full mesh structure
    readmesh3(filegrid,grid,tetra);
  
  // background and inhomogeinity DtN maps
  mat DtNbg=readDtN(fileDtNbg);

  mat DtNinclusion,DtN;
  if (diffmap) {
    DtNinclusion=readDtN(fileDtNincl);
    DtN=DtNbg-DtNinclusion;
  }
  else {
    DtN=DtNbg;
  }

  // put zeros where we do not have datas
  if (artificialless) {
    for (unsigned i=0;i<(DtN.n_rows/2);i++){
      DtN(i,i)=0; DtN(i,i)=0;   DtN(i+DtN.n_rows/2,i)=0;
    }  
  }
  
  // put noise in the DtN
  mat noisyDtN=DtN;  
  put_noise(noisyDtN,delta);
  mat err=DtN-noisyDtN;

  // Loads the computed Green's function if needed
  if (analyticgreen!=1){
    loadgreen(filegrid,dimension,G,analyticgreen);
  }


  // Construction of the indicator funcion
  vec IND(grid.size());
  IND_EIT(grid,electrodes,noisyDtN,nbpol,IND,delta,difference,spec_trunc,G);

  // Record the function
  if (dimension==3){
    writeVTK(plot_file,grid,tetra,IND );
  }
  else {
    ofstream fout;
    fout.open(plot_file.c_str());
    for (unsigned i=0;i<grid.size();i++) {
      if (dimension==2)
	fout<<grid[i].x<<" "<<grid[i].y<<" "<<IND(i)<<endl;
      else{
	fout<<grid[i].x<<" "<<grid[i].y<<" "<<grid[i].z<<" "<<IND(i)<<endl;
      }
    }
    fout.close();
  }
  
  return 0;
}
