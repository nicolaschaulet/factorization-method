function [BV,output] = complete_SVT(BVIn,noise) 

    Omega = find(BVIn~=0);
    maxiter = 3000;
    delta=1; % step size
    tol = 1e-8; 
    [U,S,V,numiter,output] = SVT([size(BVIn,1) size(BVIn,2)],Omega,...
        BVIn(Omega),1000,delta,maxiter,tol,noise);
    BV = U*S*V';
end