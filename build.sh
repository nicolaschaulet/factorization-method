#!/bin/bash

echo "--------------------------------------"
echo "Starts with armadillo's install"
echo "--------------------------------------"

cd armadillo
tar -xf armadillo-4.300.8.tar
mkdir armadillo-install
cd armadillo-4.300.8
cmake .
make
make install DESTDIR=../armadillo-install
cd ../..

echo "--------------------------------------"
echo "Armadillo : Done"
echo "--------------------------------------"


echo "--------------------------------------"
echo "Building the Factorization Method code"
echo "--------------------------------------"

cd Factorization_src
make

echo "--------------------------------------"
echo "Factorization Method : Done"
echo "--------------------------------------"