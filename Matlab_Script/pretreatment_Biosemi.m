clear all; close all;
potnumber=2;
nfreq=1; 
freq=[225,1025,1575];

% Load Data
root='/Users/nicolas/Recherche/Codes/EIT/Factorization/RealData/Biosemi_05092013/raw_BV/';
proto=load('/Users/nicolas/Recherche/Codes/EIT/Factorization/RealData/Rat_Camille_250513/rat_tank_polar_injection_BioSemi_4x32_v2.txt');
Base=load([root,'BV_Baseline',int2str(potnumber),'_',int2str(freq(nfreq)),'.mat' ]);
Incl=load([root,'BV_Plastic',int2str(potnumber),'_',int2str(freq(nfreq)),'.mat']);
NameBase=fieldnames(Base);
NameIncl=fieldnames(Incl);
BVBase=getfield(Base,NameBase{1});%NameBase{potnumber+3*(freq-1)});
BVIncl=getfield(Incl,NameIncl{1});%NameIncl{freq});


BVdiff1=BVIncl-BVBase;
BVdiff=[BVdiff1(:,1:8),BVdiff1(:,21:27)];
for i=1:size(BVdiff,1)
    av=abs(mean(BVdiff(i,:)));
    BVdiff(i,find(abs(BVdiff(i,:))>20*av))=0;
    %BVdiff(i,proto(i,2:3))=0;
    BV=BVdiff;
end

 [BVdif,normtab]=complete_svd((BVdiff));
 for i=1:size(BVdiff,1)
     BVdiff(i,:) = BVdiff(i,:)-mean(BVdiff(i,:));
 end
 plot(log(normtab))



[I,J,D]=vectomat(BVBase',15,29);
fid = fopen([root,'IN/',NameBase{1},'.txt'],'w');
fprintf( fid,'%d\n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
[I,J,D]=vectomat(BVIncl',15,29);
fid = fopen([root,'IN/',NameIncl{1},'.txt'],'w');
fprintf( fid,'%d\n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
[I,J,D]=vectomat(BVdiff',15,29);
fid = fopen([root,'IN/',NameBase{1},'diff.txt'],'w');
fprintf( fid,'%d\n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);




