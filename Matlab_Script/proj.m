function x=proj(b,A)
% computes the projection of b on the raws of A
B=A'*A;
x=zeros(size(b));
rn=x;
r0=ones(size(x));
count=0;
while ((norm(rn-r0)>10e-6)||(count<100))
    r0=rn;
    rn=b-x;
    y=B^-1*A'*rn;
    x=x+A*y;
    count=count+1;
end

end