function [B] = read_ND_ff(DtNfile)
% reads the DN file out of FF++
% and returns a matrix
    in = fopen(DtNfile,'r');
    nbmeas = fscanf(in,'%i\n',1);
    A =fscanf(in,'%i %i %f',[3 nbmeas]);
    B = full(sparse(A(1,:)'+1,A(2,:)'+1,A(3,:)'));
end
    