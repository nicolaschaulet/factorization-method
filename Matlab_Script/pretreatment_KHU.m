% To put data from the KHU in the right format
function [A]= pretreatment_KHU(incfile,bgfile,saveincl,savebg)
root='';
freqmax=1;
protocol='maxDist_Tugba_tank';
pp=load([protocol,'.prt']);
nbelec=32;
nmeas=(nbelec-4);
nbcurrent=floor(size(pp,1)/nmeas);

% change of basis matrix
proto_m=zeros(nbcurrent*nbelec,size(pp,1));
countchannel=0;
for i=1:nbcurrent
    % the two injecting electrodes for current number i
    i1=pp(countchannel+1,1); 
    i2=pp(countchannel+1,2);
    if ((i1==1)&&(i2==32))||((i2==1)&&(i1==32))
        a=ones(nbelec-1,1);
        ptem=diag(a,+1);
        ptem(32,1)=1;
        index = true(1, size(ptem, 1));
        index([2,32,1]) = false;
        proto_m(((i-1)*nbelec+1):i*nbelec,countchannel+1:countchannel+29)=ptem(:,index);   
        countchannel=countchannel+29;
    else
        a=ones(nbelec-1,1);
        ptem=diag(a,+1);
        ptem(32,1)=1;
        index = true(1, size(ptem, 1));
        if (i1+1)==33
          i3=1;
        else
          i3=i1+1;
        end
        if (i2+1)==33
          i4=1;
        else
          i4=i2+1;
        end
        index([i1,i3, i2,i4]) = false;
        proto_m(((i-1)*nbelec+1):i*nbelec,countchannel+1:countchannel+28)=ptem(:,index);
        countchannel=countchannel+28;
    end
end


freq=1;

% load of the background data
load([root,bgfile]);
% Average over frames
data=reshape(data,[size(data,2),size(data,3)]);
data_av=mean(data,2);
bg=proto_m*real(data_av);


% load odata with inclusion
load([root,incfile]);
% Average over frames
data=reshape(data,[size(data,2),size(data,3)]);
data_av=mean(data,2);
incl=proto_m*real(data_av);

test=incl-bg;
a=abs(sum(test/size(test,1)));
%incl(abs(test)>a)=0;
%bg(abs(test)>a)=0;

% Save the matrices
[I,J,D]=vectomat(bg,nbelec,nbcurrent);
fid = fopen(savebg,'w');
fprintf( fid,'%d \n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
A=sparse(I,J,D);
[I,J,D]=vectomat(incl,nbelec,nbcurrent);
fid = fopen(saveincl,'w');
fprintf( fid,'%d \n',size(I,1));
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
A=A-sparse(I,J,D);
A=full(A);
end

% Convert a raw vector to a n x m sparse matrix
function [I,J,D]=vectomat(M,n,m)
count=1;
I=zeros(n*m,1);
D=zeros(n*m,1);
J=zeros(n*m,1);
for j=1:m  
    for i=1:n
        I(count)=i;
        J(count)=j;
        D(count)=M(count);
        count=count+1;
    end
end 
end