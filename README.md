# FM-EIT #

This piece of software implements the Factorization Method for solving the obstacle 
problem in Electrical Impedance Tomography  (see [article](http://arxiv.org/abs/1312.1479) 
and references therein for further details).

* Status : Beta

## Content ##

In addition to C++ code implementing the **Factorization Method** for solving the obstacle problem in EIT, the repository also contains some Matlab helpers, geometry files, scripts to compute the Neumann Green's function and examples:

1. **Factorization_src**: source code for the factorization method
2. **armadillo**: linear algebra library [Armadillo](http://arma.sourceforge.net)
2. **MeshesAndCo**: geometry files for 3D examples 
3. **Matlab_Script**: Matlab helpers including visualisation tools
5. **ProcessMesh**: Helpers to deal with the meshes
6. **Data_Examples**: Simulated data files corresponding to geometries provided in `MeshesAndCo`
4. **Compute_Green**: [FreeFem++](http://www.freefem.org/ff++/) scripts to compute the Green's function in 2D and 3D -> send me an email to get information about that

## Install ##

1. Install [cmake](http://www.cmake.org)

2. Get the source code:

        > git clone -b public https://bitbucket.org/nicolaschaulet/factorization-method.git
        > cd factorization-method


3. Build:

        > sh build.sh

4. If you want to only build the FM source code:

        > cd Factorization_src
        > make


## How to use

The configuration file is called `data_factorization.dat` and should be placed in the main folder (not with the source code). An example of this file is provided in `data_factorization.dat.template`.

**/!\ Do not change the structure and ordering of the file!**

Once this file is edited, simply run
	
	> ./Factorization

and it will generate and save (see next section) the indicator function of the particular inclusion that is being imaged.


## Example

1. Copy the `Data_Example/data_factorization.dat` and paste it into the main directory (where the `Factorization` executable file is located)
2. Launch the reconstruction
	
		> ./Factorization


3. Visualise the result `Data_Examples/head_skull_2.vtk` with [Paraview](http://www.paraview.org) or by using the `plotIND3d.m`
script in the `Matlab_Script` folder.


Other examples are available in the `Data_Examples` forlder, please refer to the name to find the according 
geometry file (i.e. `cylinder_2_1.bg` corresponds to the geometry `cylinder_2.mesh`).
## Data structure

### Main input/output
As input the program takes mainly 5 data files:

- `geom.mesh`: structured (or unstructured in 2D) mesh file that contains the grid on which the indicator function is built
- `geom.elec`: position of the electrodes, usually associated with the `geom.mesh` file
- `file.bg`: file that contains the voltages for **the baseline** on each electrode (put 0 if missing entry)
- `file.incl`: contains the voltages **with perturbation** on each electrode (put 0 if missing entry)
- `geom.mesh.green` : contains the vector test funcion corresponding to the mesh `geom.mesh` evaluated at the points given in`geom.mesh`. Used only if `Analytic Green's function available` is set to 0 in the parameter file

and it produces one output: 

- `plot.vtk`: contains the value of the indicator function on the grid `geom.mesh`

Some examples of these files (for the 3D case) can be found in the `MeshesAndCo` folder.


### Formatting

#### geom.mesh
The format of this file changes between dimension 2 and 3. 

In **dimension 2** the file simply consists in a list of points (x and y coordinates) preceeded by the number of point.

**Example:**

	3
	1 1
	1 0
	0 0


In **dimension 3** it is a tetrahedral structured mesh file that contains first
	
	MeshVersionFormatted 1
	Dimension 3

Then the keyword **Vertices** is followed by the number of vertices and the list of vertices

	Vertices
	NumberOfVertices
	x1 y1 z1
	   ...
	xn yn zn

Finally comes the keyword **Tetrahedra** followed by the number of Tetrahedra and the list of tetrahedra with a label (0 by default)

	Tetrahedra
	NumberOfTetrahedra
	v1 v2 v3 0
		...

The file can also contain the boundary of the mesh that is a list of triangles that follows the keyword **Triangles**.
 See examples in `MeshesAndCo`. 

The `ProcessMesh/loadmesh.m` Matlab function can be used to import such structured meshes into Matlab. Similarly, `ProcessMesh/savemeshff.m` can be used to export a Matlab mesh into the appropriate text file. 


#### file.elec

This file contains a list of electrode positions and the format is the same in dimension 2 and 3:

	NumberOfElectrodes
	x1 y1 z1
	   ...
	xn yn zn

In dimension 2 there is no z coordinate.

#### file.bg and file.incl

Contains the so-called Neumann-to-Dirichlet map discretised on the electrodes given in `geom.elec`. 
We represent the map (a matrix in that case) as a sparse matrix. The file contains first the number of 
non zero entries followed by the list of the non zero entries in the sparse formart: 

	i j M_{i,j}

if `M_{i,j}` is the entry (i,j) of matrix M.
Examples of such files can be found in the `Data_Examples` folder


#### geom.mesh.green

Contains the gradient of the Neumann Green's function corresponding to the mesh `geom.mesh` that is used if the 
`Analytic Green's function available` option is set to 0 in the parameter file. Otherwise that file is not needed.



#### plot.vtk
In **dimension 2** the ouput contains only a list of points (the one given in the `geom.mesh` file)
and the value of the indicator function **Ind** at that particular point:

	x1 y1 Ind(x1,y1)
		...
	xn yn Ind(xn,yn)

The file can be displayed by using the `Matlab_Script/plotIND.m` function.

In **dimension 3** the output file is a propoer [vtk file](http://www.vtk.org) that contains once more the value of the indicator function
on the mesh given in `geom.mesh`. It can be visualised by used [Paraview](http://www.paraview.org) or by using the `Matlab_Script/plotIND3d.m` function.


## License

This code is distributed under an [MIT license](http://nico.mit-license.org).
 Notice that the library [Armadillo](http://arma.sourceforge.net) is distributed under a
  [Mozilla Public License](http://www.mozilla.org/MPL/2.0/).






