function [Surf]=savemeshff(tetrahedra,vertices,region,electrodes,filename)
    Surf=dubs3_2(tetrahedra);
    inpfile = fopen(filename,'w');


    % Write Heading and Nodes
    fprintf(inpfile,'MeshVersionFormatted 1\n\n');
    fprintf(inpfile,'Dimension 3\n\n');
    fprintf(inpfile,'Vertices\n');
    fprintf(inpfile,[num2str(size(vertices,1)) '\n']);
    vertpr=[vertices,zeros(size(vertices,1),1)];
    fprintf(inpfile,'%f %f %f %i\n',vertpr');

    % Write Tetrahedra
    fprintf(inpfile,'\nTetrahedra\n');
    fprintf(inpfile,[num2str(size(tetrahedra,1)) '\n']);
    tetpr=[tetrahedra,region];
    fprintf(inpfile,'%i %i %i %i %i\n',tetpr');


    % Write Boundary triangles
    fprintf(inpfile,'\nTriangles\n');
    fprintf(inpfile,[num2str(size(Surf,1)) '\n']);
    label=findtribis(Surf,electrodes);
    surfpr=[Surf,label];
    fprintf(inpfile,'%i %i %i %i\n',surfpr');

    % Final line
    fprintf(inpfile,'\nEnd\n');
    fclose(inpfile);
end