function [] = listtovtk(  )
%LISTTOVTK Summary of this function goes here
%   Detailed explanation goes here

    f = fopen('test.vtk','w');
    fprintf(f,'# vtk DataFile Version 2.0\n' );
    fprintf(f,'Data from factorization\n');
    fprintf(f,'ASCII\n');
    filename = 'Test/head.txt';
    A=dlmread(filename);
    point = A(:,1:3);
    ind = A(:,4);
    clear A;
    fprintf(f,'DATASET POLYDATA\n');
    fprintf(f,'POINTS %d float\n',size(point,1));
    fprintf(f,'%f %f %f\n',point');
    fprintf(f,'POINT_DATA %d\nSCALARS element float\nLOOKUP_TABLE default\n',size(ind,1));
    fprintf(f,'%f\n',ind);
    
end

