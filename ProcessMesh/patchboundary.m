function [] = patchboundary(vertices,tetrahedra,electrodes,region,filepath,filename)
% Function to out electrodes on the entire boundary -> used to compute the
% Green's function
%
%   vtx : list of all vertices of the mesh
%   tetra : tetrahedra of the emsh
%   electrodes : list of the center of all electrodes (nbelec x 3) 
%   filepath : path for the savings
%   filename : root filename (ex: rat, cylinder1 ...)

Surf=dubs3_2(tetrahedra);

inpfile = fopen([filepath filename 'patched.mesh'],'w');

% Write Heading and Nodes
fprintf(inpfile,'MeshVersionFormatted 1\n\n');
fprintf(inpfile,'Dimension 3\n\n');
fprintf(inpfile,'Vertices\n');
fprintf(inpfile,[num2str(size(vertices,1)) '\n']);
vertpr=[vertices,zeros(size(vertices,1),1)];
fprintf(inpfile,'%f %f %f %i\n',vertpr');

% Write Tetrahedra
fprintf(inpfile,'\nTetrahedra\n');
fprintf(inpfile,[num2str(size(tetrahedra,1)) '\n']);
tetpr=[tetrahedra,region];
fprintf(inpfile,'%i %i %i %i %i\n',tetpr');


% Write Boundary triangles
fprintf(inpfile,'\nTriangles\n');
fprintf(inpfile,[num2str(size(Surf,1)) '\n']);
label=findcloserelec(Surf,vertices,electrodes); 
surfpr=[Surf,label];
fprintf(inpfile,'%i %i %i %i\n',surfpr');

fprintf(inpfile,'\nEnd\n');

fclose(inpfile);

end



function [d]=distt(x,y)
d = sqrt(sum((x-y).^2));
end

function [label] = findcloserelec(Surf,vtx,elec)
% finds the closer electrode for each triangle in Surf
    numelec=size(elec,1);
    label=zeros(size(Surf,1),1);
    for (i=1:size(Surf,1))
        barycentre=1/3*[sum(vtx(Surf(i,:),1)),sum(vtx(Surf(i,:),2)) ...
            ,sum(vtx(Surf(i,:),3))]; % barycenter of the actual triangle
        distelec=distt(barycentre,elec(1,:));
        label(i)=1;
        for (kk=2:numelec) % loop to find the closer electrode
            distelectt=distt(barycentre,elec(kk,:));
            if (distelectt<distelec)
               distelec=distelectt;
               label(i)=kk; 
            end
        end
    end   
end

