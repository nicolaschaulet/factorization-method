////////////////////////////////////////////////////////////
//  Various algorithms for the factorization method
// N. Chaulet

#include "algos.hpp"


void IND_EIT(const vector<point> & grid, const vector<point> &electrodes,
	     const mat & DtN,const int & nbpol,vec & out,const double &delta,
	     const bool &difference,const int & spec_trunc, const vector<vector<point> > &G)
{
 // Various variables
  vec phi,secmb,s;
  mat U,V;
  double vecnorm;
  point Z,pol;
  int nbdisc=grid.size();
  out.resize(nbdisc);
  double alpha=0; // Tikhonov parameter
  mat Xsym=DtN;
  int npol=nbpol;

  // Analytic or computed Green's function
  bool analyticgreen=(G.size()==0);

  // SVD of the DTN map 
  mat Ut,Vt; vec st;
  svd(Ut,st,Vt,Xsym);
  cout<<"Singular values of the difference operator ="<<endl;
  st.raw_print();

  // Computation of the level of noise on Ld^{1/2}
  // Spectral truncation
  unsigned trunc=0;
  if (spec_trunc>0)
    trunc=spec_trunc;
  else    
    trunc=trunclevel(st,1e-10);
  U=Ut.cols(0,trunc);
  V=Vt.cols(0,trunc);
  s=st.rows(0,trunc);

  // Build the indicator function
  for (int i=0;i<nbdisc;i++) {
    Z=grid[i];
    out(i)=0;
    if (Z.dimension==3)
      npol=8; // Take 7 for the cylinder 8 if not the cylinder
    for (int pp=0;pp<npol;pp++) {
      if (Z.dimension==2)
	pol=point(cos(datum::pi/double(nbpol)*pp),sin(datum::pi/double(nbpol)*pp)); 
      else {
	if (pp<3)
	  pol=point(cos(datum::pi/3*pp),sin(datum::pi/3*pp),0);
	if ((pp>2)&(pp<7))
	  pol=point(cos(2*datum::pi/4*(pp-2)),sin(2*datum::pi/4*(pp-2)),1/sqrt(2.));
	if (pp==7)
	  pol=point(0,0,1);
      }
      pol=pol/(pol.norm());
      if (analyticgreen) {
	phi=computegreen(electrodes, Z, pol);
	}
      else {
	phi.resize(electrodes.size());
	for (unsigned j=0;j<electrodes.size();j++) {
	  phi(j)=G[i][j]*pol;
	}
      }
  phi = phi - mean(phi);
      if (difference){ // the data corresponds to the voltage difference between two electrodes
	mat P=abs_to_dif(phi.n_rows);
	phi=P*phi;
      }
      secmb=U.t()*phi;
      alpha=0;//morozov(secmb,s,eps);
      secmb=secmb%secmb;
      vecnorm=norm(secmb,1);
      secmb=secmb/vecnorm;
      secmb=(s/(alpha+pow(s,2)))%secmb;
      out(i)+=norm(secmb,2);//average over polarizations pol
    }
    out(i)=1./log(1+out(i)); 
    //out(i)=1./out(i);

  }

  // Renormalisation between 0 and 1
  out = out - out.min();
  out = out/out.max();
  
  return ;
}

// determines the truncation index
unsigned trunclevel(const vec & st, const  double & le)
{
  unsigned tt=0;
  for (tt=0;tt<st.n_rows;tt++){
    if (st(tt)<le) break;
  }
  return tt-1;
}

// To interpolate the the column of M with average of neighbours
void completehalf(mat & M)
{
  int n=M.n_rows; int m=M.n_cols;
  for (int j=0;j<m;j++){
    if (M(0,j)==0)
      M(0,j)=(M(1,j)+M(n-1,j))/2;
    if (M(n-1,j)==0)
      M(n-1,j)=(M(n-2,j)+M(0,j))/2;
    for (int i=1;i<n-1;i++){
      if (M(i,j)==0)
	M(i,j)=(M(i+1,j)+M(i-1,j))/2;
    }
  }
  return;
}

// complete the columns of M by interpolation in the Fourier basis
void completetrigo(mat & M,const int & N_regul)
{
  int n=M.n_rows; int m=M.n_cols;
  int N_max=0;
  if (N_regul>n)
    N_max=n;
  else
    N_max=N_regul;
  
  int count=0; // number of zeros on a column of M
  
  for (int i=0;i<n;i++) {
    if (M(i,0)==0) {count++;}}

  if (count==0)
    return;
  
  mat CS=euclFourier(n,N_max-count);
  mat CStr(n-count,N_max-count);
  vec alpha(N_max-count),secmb(n-count);
  
  for (int j=0;j<m;j++){
    CStr.zeros();
    secmb.zeros();
    int countb=0;
    for (int i=0;i<n;i++) {
      if (M(i,j)!=0) {
	secmb(i-countb)=M(i,j);
	CStr.row(i-countb)=CS.row(i);
      }
      else
	countb++;	
    }
    alpha=solve(CStr,secmb);
    M.col(j)=CS*alpha;
  }
  return;
}


// Compute the influence of the Green's function on the ith mode of the svd of the NtD
void compute_influence(const mat & DTN, const  vector<vector<point> > & G,const vector<point> & electrodes,const point & Z, vec & influence)
{
  bool analyticgreen=(G.size()==0);
  mat Ut,Vt; vec st,phi;
  mat DD=DTN;
  point pol;

  if (Z.dimension==2)
    pol=point(1,1);
  else
    pol=point(1,1,1);
  pol=pol/pol.norm();
  
  //  computes the svd of the NtD map
  svd(Ut,st,Vt,DD);
  Ut=Ut.cols(0,st.n_rows-1);
  
   //  computes the Green's function
  if (analyticgreen) {
    phi=computegreen(electrodes, Z, pol);
  }

  influence=Ut.t()*phi;
  influence=influence%influence;
  double vecnorm=norm(influence,1);
  influence=influence/vecnorm;
  influence=(1/st)%influence;
  
 //  The contributions of each frequency to the cost functional
  
  return;
}


// Computation of Morozov's parameter
double morozov(const vec &g,const vec &sigma,const double & eps)
{
  double alphamin=eps*min(sigma);
  double alphamax=eps*max(sigma);
  double tol=1e-8;
  vec F(1);
  double alpha=alphamin;

  // initialisation
  F= sum(((pow(alpha,2)-pow(eps,2))*pow(sigma,2))%pow(g,2)
    /(pow(alpha+pow(sigma,2),2)));

  // Dichotomie Loop
  while(abs(F(0))>tol) {
    alpha=(alphamin+alphamax)/2.;
    F= sum(((pow(alpha,2)-pow(eps,2))*pow(sigma,2))%pow(g,2)
	   /(pow(alpha+pow(sigma,2),2)));
    if (F(0)<=0)
      alphamin=alpha;
    else
      alphamax=alpha;
  }
  
  return alpha;
}

