
function [elec, cnts, srf_area] = set_electrodes_elements(vtx, srf, elec_vtx,n_elec, fig)
% Usage: [elec, cnts, srf_area] = set_electrodes_var_elements(vtx, srf, pos, w, method, fig);
%
% General:
% Project electrodes on the mesh surface
%
% Input:
% vtx - vertices matrix {n x 3}
% srf - external surfaces {no. of external surfaces x 3}
% pos - position of the electrodes surfaces vertices {no. of electrodes x 3}
% w - electrode's diameter {scalar}
% method  - searching for elements below electrode surface: {char}
%           'n' - starting from the node which is closest to the electrode position
%           's' - starting from the element centre which is closest to the electrode position
%           'm' - allocating the surfaces whose nodes are within radius w/2
%           from a mesh point according to the remoted-most surface cooridnate
%           'v' - allocating the surfaces whose nodes are within radius w/2
%           from a mesh pointaccording to the centres
% fig - flag, if not exist surface mesh will be ploted {} (optional)
%
% Output:
% elec - boundary surfaces vertices indices, each row contains the vertices indices for one electrode,
% arranged in groups of 3's {no. electrodes x (max no. of faces representing electrode)*3}
% cnts - coordinates of the centre of each triangular boundary surface {no. of external surfaces x 3}
% srf_area - electrodes surface area {no. of electrodes x 1}
%-------------------------------------------------------------------------------------------------------------

%Find surface elements that define the electrodes

elec_cnt = ones (n_elec,1);

c_sels = cell (n_elec,1);
sels = [];


for nel =1:n_elec;
    
    for n=1:length(srf)
        
        found = false;
        
        for i=1:size(elec_vtx,2)
            
            vtx1 = elec_vtx(nel,i);
            
            if srf(n,1) ==vtx1
                
                for j=1:size(elec_vtx,2)
                    
                    vtx2 = elec_vtx(nel,j);
                    
                    if srf(n,2) == vtx2
                        for k=1:size(elec_vtx,2)
                            
                            
                            vtx3 = elec_vtx(nel,k);
                            
                            if srf(n,3) == vtx3
                                
                                c_sels{nel}(elec_cnt(nel)) = n; %Store the index of the element in srf
                                sels = [sels c_sels{nel}];
                                elec_cnt(nel) =  elec_cnt(nel) +1;
                                
                            end
                            
                        end
                    end
                end
            end
            
        end
        
    end
end




%Remove duplicated values; calculate electrode area
for j = 1:n_elec
    c_sels{j} =  unique(c_sels{j});
     cnts(j,:) = mean(vtx(srf(c_sels{j},:),:));
     no_el_srf(j) = length(c_sels{j}); % stores the no of surfaces for each electrode
     srf_area (j) = 0; % initilize the total surface area of the electrode
        for jj = 1:no_el_srf(j)
            V = vtx(srf(sels(jj),:)',:);
            [ta] = triarea3d(V); % calculate the the surface area of the surface
            srf_area(j) = srf_area(j) + ta;
        end

end

%Create array with the electrode vertices
elec = zeros(n_elec,max(no_el_srf)*3);

for j=1:n_elec
    elec(j,1:no_el_srf(j)*3) = reshape(srf(c_sels{j},:)',1,no_el_srf(j)*3);
end
%----------------------------------------------------------------------
sels=sels';



%-------------------------------plot the electrodes---------------------------------------
if ~exist('fig')
    figure, title('electrodes');
    trimesh(srf,vtx(:,1),vtx(:,2),vtx(:,3),'FaceAlpha',0.5);
    colormap([0 0 0]);
    daspect([1 1 1]);

    hold on;
    axis image;
    set(gcf,'Colormap',[0.6 0.7 0.9]);

    grid off
    view(3);

end
hold on

for u = 1:size(sels)
    paint_electrodes(sels(u),srf,vtx);
end




% % add numbers to the electrodes
% el_caption = 1:size(n_elec,1);
% hold on;
% for i = 1:size(pos,1)
%     text(pos(i,1),pos(i,2),pos(i,3),num2str(el_caption(i)),'FontWeight','Bold','FontSize',16,'Color',[0.4 0.2 0.65]); % plots numbers on electrode pos
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This might become a part of the EIDORS suite
% Copyright (c) Lior Horesh 2004, EIT group, Medical Physics and Bioengineering, UCL, UK
% Copying permitted under terms of GNU GPL
% See enclosed file gpl.html for details
% EIDORS 3D version 2
% MATLAB Version 6.1.0.450 (R12.1) on PCWIN
% MATLAB License Number: 111672
% Operating System: Microsoft Windows Server 2003 Standard Edition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%