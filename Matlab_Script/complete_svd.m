function [BV,normtab] = complete_svd(BVIn) 
[I,J] = find(BVIn==0);
k=1;
norm0=0;norm1=10;
itemax=2000;
tol=1e-8;
ite=0;
normtab=[];
kk=[];
BV1=BVIn;
BV=BVIn;
normtab=[];
while (ite<itemax)&&(k<rank(BV))
    if (abs(norm1-norm0)>tol)
        norm0=norm1;
        [u,s,v]=svd(BV);
        BVk=u(:,1:k)*s(1:k,1:k)*v(:,1:k)';
        for  j=1:size(I,1)
            BV1(I(j),J(j))=BVk(I(j),J(j));
        end
        norm1=norm(BV-BV1,'fro');
        BV=BV1;
    else
        k=k+1;
        norm1=norm0+2*tol;
    end
    normtab=[normtab,norm0];
    ite=ite+1;
end
if (ite==itemax)
    disp 'Rich max number of iteration for data completion'
end
end
