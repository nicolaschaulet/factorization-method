function [mesh] = loadmesh(mesh_file)
% Takes a file in the mesh format and gives a mesh structure:
% mesh.vtx (Nx4) contains the vertices
% mesh.tetra (Mx5) contains the tetrahedron with region number
% mesh.boundary (Mbx4) contains the boundary elements with label
    
    fidex=fopen(mesh_file,'r');
    line = fgets(fidex);
    if (isempty(strfind(line,'MeshVersionFormatted 1')))
         error('Not the rigth mesh format!!');
    end
    
    while (feof(fidex)==0)
        line = fgets(fidex);
        
        % First Load Vertices
        if (~isempty(strfind(line,'Vertices')))
            nv=fscanf(fidex,'%f',1);
            vtx = fscanf(fidex,'%f',[4,nv]);
            mesh.vtx=vtx';
        end
        
        % Then load tetrahedron
        if (~isempty(strfind(line,'Tetrahedra')))
            nt=fscanf(fidex,'%f',1);
            tetra = fscanf(fidex,'%f',[5,nt]);
            mesh.tetra=tetra'; 
        end
        
        % Finally loads boundary triangles
        if (~isempty(strfind(line,'Triangles')))
            ntri=fscanf(fidex,'%f',1);
            tri = fscanf(fidex,'%f',[4,ntri]);
            mesh.boundary=tri';
        end
    end
    
end