%close all
clear all
root='Carrot_Emma_08032013/';
freqmax=27;

Iin=133*10^-6;
for freq=1:freqmax

% Careful, this is only for 32 protocol with 16 current injections

% load of the protocol matrix
load(['Potatoe_Tubga_06032013/electrode_protocol/pol_32.prt']);
In=pol_32(:,1:2);
OUT=pol_32(:,3:4);
meas=size(In,1);
proto=zeros(32*16,32);
proto_m=zeros(32*16,28*16);
j=2;

for (i=0:15)
    count=1;
    ct=1;
    if (i>0)
        ct=0;
        j=1;
    end
    while count<29
        igl=i*28+count;
        jgl=i*32+j;
        if (In(igl,1)==j+1)||(In(igl,2)==j+1)
            j=j+2;
            ct=ct+1;
        else
            if (i>1)
                proto_m(jgl,i*28+rem(28+count-(i),28)+1)=1;
            else
                proto_m(jgl,i*28+count)=1;
            end
            proto(jgl,j)=-1;
            proto(jgl,rem(j,32)+1)=1;
            j=j+1;
            count=count+1;
        end
    end
end


% load of the background data
load([root,'background_carrot.mat']);

% Average over frames
data_av(:,:)=1/size(data,3)*sum(data,3);
bg=proto_m*real(data_av(freq,:))';
test=real(data_av(freq,:))';


% load of the background data
load([root,'perturbation_carrot2.mat']);

% Average over frames
freq_meas=25;
data_av(:,:)=1/size(data,3)*sum(data,3);
load([root,'carrot.mat']);
incl=proto_m*real(carrot_trunc(freq)*data_av(freq,:))';
%incl=proto_m*real(data_av(freq,:))';
bg=proto_m*real(carrot_trunc(freq_meas)*data_av(freq_meas,:))';




[I,J,D]=vectomat(bg);
fid = fopen([root,'IN/bg_',num2str(freq-1),'.txt'],'w');
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
[I,J,D]=vectomat(incl);
fid = fopen([root,'IN/incl_',num2str(freq-1),'.txt'],'w');
fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
fclose(fid);
end

% Difference NtD map
% incl=proto_m*real(carrot_trunc(7)*data_av(7,:))';
% bg=proto_m*real(carrot_trunc(14)*data_av(14,:))';
% [I,J,D]=vectomat(-bg+incl);
% Aexp=full(spconvert([I,J,D]));


% 
% Transformation of synthetic data to make them as the experimental ones
% 
%  load '/Users/nicolas/Recherche/FreeFem/codes/EIT/2DFullImpedanceModel/OUT/resistivitybg.txt'
%  load '/Users/nicolas/Recherche/FreeFem/codes/EIT/2DFullImpedanceModel/OUT/resistivityincl.txt'
%  resistivitybg(:,1:2)=resistivitybg(:,1:2)+1;
%  resistivityincl(:,1:2)=resistivityincl(:,1:2)+1;
%  A=full(spconvert(resistivitybg)-spconvert(resistivityincl));
%  for (i=1:16)
%     Aout=proto(1+(i-1)*32:(i)*32,:)*A(:,i);
%     A(:,i)=Aout;
% end
% [I,J,D]=vectomat(A);
% fid = fopen('testreal.txt','w');
% fprintf( fid,'%d %d %f\n',[(I-1)';(J-1)';D'] );
% fclose(fid);
% 
% A=A/norm(A)*norm(Aexp);
% % Various plots
% for i=1:16
%     figure
%     plot(A(:,i));
%     hold on
%     plot(Aexp(:,i),'r');
% end





