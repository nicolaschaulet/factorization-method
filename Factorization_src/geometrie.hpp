////////////////////////////////////////////////////////////////////////
//  Include file for points and mesh functions
//    N. Chaulet

#ifndef geom_h
#define geom_h

#include "main.hpp"
using namespace std;
using namespace arma;




class point {
public :
  double x;
  double y;
  double z;
  int dimension;
  point() {x=0;y=0;z=0;};
  point(double _x,double _y,double _z) {x=_x;y=_y;z=_z;dimension=3;};
  point(double _x,double _y) {x=_x;y=_y;z=0;dimension=2;};
  point(const point & pp) {x=pp.x;y=pp.y;z=pp.z;dimension=pp.dimension;};
  friend ostream & operator <<(ostream & O, const point & pp) {O<<pp.dimension<<endl<<pp.x<<" "<<pp.y<<" "<<pp.z;return O;};
  point & operator=(const point & P1) {dimension=P1.dimension;x=P1.x;y=P1.y;z=P1.z;return *this;};
  double norm(){return(sqrt(pow(x,2)+pow(y,2)+pow(z,2)));};
};


// External operators of point class
inline point operator-(const point &P1,const point &P2) {
  assert(P1.dimension==P2.dimension);
  point p; p.dimension=P1.dimension; p.x=P1.x-P2.x;p.y=P1.y-P2.y; p.z=P1.z-P2.z;return p;
}
inline point operator+(const point &P1,const point &P2) {
  assert(P1.dimension==P2.dimension);
  point p; p.dimension=P1.dimension; p.x=P1.x+P2.x;p.y=P1.y+P2.y; p.z=P1.z+P2.z;return p;
}
inline double operator*(const point &P1,const point &P2) {
  assert(P1.dimension==P2.dimension);
  double sc=P1.x*P2.x+P1.y*P2.y+P1.z*P2.z;return sc;
}
inline point operator/(const point &P1,const double &a) {
  point p(P1);
  p.x=p.x/a;p.y=p.y/a;p.z=p.z/a;return p;
}

// reading of the geometry 
void readpoints(const string &,vector<point> &); // only in 2D
void readmesh3(const string &,vector<point> &,vector<vector<int > > &); // only in 3D
void writeVTK(const string & file,vector<point> const &vtx,vector<vector<int > > const &tetra,const vec ind ); // Write to vtk in 3D

// construction of the right hand sides  
vec  computegreen(const vector<point> &, const point &,const point & );
void loadgreen(const string &filegeo,const int &dimension,vector<vector<point> > &G, const int &tag);
void ffcomputegreen(const string & filemesh,const string &filegeo, const string & fileelec,vector<vector<point> > &G,const int &dimension);
#endif /* geometrie.hpp */



/*
class point2D {
public :
  double x;
  double y;
  point2D() {x=0;y=0;};
  point2D(double _x,double _y) {x=_x;y=_y;};
  point2D(const point2D & pp) {x=pp.x;y=pp.y;};
  friend ostream & operator <<(ostream & O, const point2D & pp) {O<<pp.x<<" "<<pp.y;return O;};
  point2D & operator=(const point2D & P1) {x=P1.x;y=P1.y;return *this;};
  double norm(){return(sqrt(pow(x,2)+pow(y,2)));};
};

// External operators of point2D
inline point2D operator-(const point2D &P1,const point2D &P2) {
  point2D p; p.x=P1.x-P2.x;p.y=P1.y-P2.y;return p;
}
inline double operator*(const point2D &P1,const point2D &P2) {
  double sc=P1.x*P2.x+P1.y*P2.y;return sc;
}
void readpoints(const string & ,vector<point2D> &);
vec  computegreen(const vector<point2D> &, const point2D &,const point2D & );
void ffcomputegreen(const string & filemesh,const string &filegeo, const string & fileelec,vector<vector<point2D> > &G);
*/
