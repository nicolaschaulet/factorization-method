function [] =  printelec(Mesh,pos)
    trimesh(Mesh.srf,Mesh.vtx(:,1),Mesh.vtx(:,2),Mesh.vtx(:,3),'FaceAlpha',0.5);
    colormap([0 0 0]);
    daspect([1 1 1]);
    hold on
    for i=1:size(pos,1)
        text(pos(i,1),pos(i,2),pos(i,3),num2str(i),'FontWeight','Bold','FontSize',16,'Color',[0.4 0.2 0.65]);
    end
end