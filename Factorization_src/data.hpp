////////////////////////////////////////////////////////////
// Data reading for the factorization method
// N. Chaulet

#ifndef data_h
#define data_h

#include "main.hpp"

using namespace std;

void read_data(const string &,vector<string> &,double *); // reads  the data for the factorization method

#endif /* data.hpp */
