////////////////////////////////////////////////////////////
//  Various algorithms for the factorization method
// N. Chaulet


#ifndef algo_h
#define algo_h

#include "main.hpp"
#include "geometrie.hpp"
#include "matrixpart.hpp"
using namespace std;
using namespace arma;


void IND_EIT(const vector<point> &,const vector<point> &,const mat &,const int &,vec &,const double &,const bool&,const int &, const vector<vector<point> > &G );
unsigned trunclevel(const vec & , const  double &);
void completehalf(mat &A); // complete the vectors contained in matrix A by taking the average between the adjacent values
void completetrigo(mat & A,const int & N_max); // complete the vectors contained in matrix A by using an interpolation in the trigo basis with truncation at n=N_max
void compute_influence(const mat & DTN, const  vector<vector<point> > & G,const vector<point> & electrode, const point &Z, vec & influence);


// compute Tikhonov-Morozov's parameter
double morozov(const vec &g, // right-hand side times U
	       const vec &sigma, // singular values
	       const double &eps); // noise level

#endif /* algos.hpp*/


/*
  void IND_EIT(const vector<point2D> &,const vector<point2D> &,const mat &,const int &,vec &,const double &,const bool&,const int &, const vector<vector<point2D> > &G );
*/
