function []=plotisoline(Mesh,f,eps)
% plot the isovalue eps*max(|f|) on the mesh Mesh
  gcf=figure;
  srf=dubs3_2(Mesh.tri);
  p = patch('Vertices', Mesh.vtx, 'Faces', srf);
  set(p, 'FaceColor', 'black', 'EdgeColor', 'none'); 
  alpha(0.1);
  hold on
  
  if nargin < 5 
    x1 = 'X' ; x2 = 'Y' ; x3 = 'Z';
  end;	 
  if eps > 1
    disp([' '])
    disp(['la valeur du deuxieme argument doit etre < 1'])
    disp([' '])
    error('........................... Stopped')
  end;

    ind=abs(f);
    point = barycentre(Mesh);
    dx=0.001;
    x_edge=[min(point(:,1)):dx:max(point(:,1))];
    y_edge=[min(point(:,2)):dx:max(point(:,2))];
    z_edge=[min(point(:,3)):dx:max(point(:,3))];
    [X,Y,Z]=meshgrid(x_edge,y_edge,z_edge); 
    F= TriScatteredInterp(point(:,1),point(:,2),point(:,3),ind(:));
    Zex=F(X,Y,Z);

  % %%%%%%%%%%%%%%%%%%%% 3D RECONSTRUCTION %%%%%%%%%%%%%%%%

      % %%%%%%%%%%%%
      vmax = max(max(max(Zex)));
      isov = eps*vmax;
      vmin = min(min(min(Zex)));
      if isov < vmin
	disp([' '])
	disp(['Pas de surfaces correspondant a la valeur',...
	      ' de coupure indiquee'])
	disp([' '])
	disp(['Valeur minimale a utiliser > ', num2str(vmin/vmax)])
	disp([' '])
	error('........................... Stopped')
      end;
	
      % %%%%%%%%%%%%
      
%      figure
      
      comm = sprintf('[F,V]=isosurface(%s,%s,%s,Zex, isov);',x1,x2,x3);
      eval(comm);
      p = patch('Vertices', V, 'Faces', F);
      set(p, 'FaceColor', 'red', 'EdgeColor', 'none');
      
% %%%
      daspect([1 1 1]) ;
            view(30,-38)   ;
     % view(3) ;
      camlight; 
      lighting phong;

% %%%

      set(gca, 'FontSize',12);
      set(gca, 'LineWidth', 1.5);
      grid on
%      set(gca, 'GridLineStyle', '-');
      
% %%%  The axis

      comm = sprintf('xmax = max(max(max(%s)));',x1);
      eval(comm);
      comm = sprintf('xmin = min(min(min(%s)));',x1);
      eval(comm);
      comm = sprintf('ymax = max(max(max(%s)));',x2);
      eval(comm);
      comm = sprintf('ymin = min(min(min(%s)));',x2);
      eval(comm);
      comm = sprintf('zmax = max(max(max(%s)));',x3);
      eval(comm);
      comm = sprintf('zmin = min(min(min(%s)));',x3);
      eval(comm);

      %xmin=-2.; xmax=0.5; ymin=-0.5; ymax=0.5; zmin=-0.5; zmax=0.5;
      %disp('Warning: axis est mis en dur dans le programme!')
      axis([xmin xmax ymin ymax zmin zmax])
% %%%
% %%% We put the projections
% %%%
      hold on
      %
      XV=V(:,1);YV=V(:,2);ZV=V(:,3);
      clear V
      %
      VXZ(:,1)=XV;VXZ(:,2)=ymax*ones(size(YV)); VXZ(:,3)=ZV;
      patch('Vertices', VXZ, 'Faces', F, 'FaceVertexCData', [1. 1. 1.], ...
      'FaceColor', 'flat','EdgeColor', 'none');
      %
      VXZ(:,1)=xmin*ones(size(XV)); VXZ(:,2)=YV; VXZ(:,3)=ZV;
      patch('Vertices', VXZ, 'Faces', F, 'FaceVertexCData', [1. 1. 1.], ...
      'FaceColor', 'flat','EdgeColor', 'none');
      %
      VXZ(:,1)=XV; VXZ(:,2)=YV; VXZ(:,3)=zmax*ones(size(ZV));
      patch('Vertices', VXZ, 'Faces', F, 'FaceVertexCData', [1. 1. 1.], ...
      'FaceColor', 'flat','EdgeColor', 'none');
      clear VXZ  V F

% %%%%
      drawnow 
      xlabel('x');ylabel('y');zlabel('z');

      %saveperso(gcf,'Figures/rat_simu2');
  end
% %%%%
      
%% To save a plot
function [] =saveperso(axiss,filepdf)
  set(axiss, 'PaperPosition', [0 0 5 5]);
  set(axiss,'papersize',[5 5]);
  saveas(axiss,[filepdf,'.pdf']);
end 



function [srf] = dubs3_2(tri)
% Usage: [srf] = dubs3_2(tri);
%
% General:
% Auxiliary function that extract the boundary faces of a given 3D volume (faster than the original dubs3.m)
%
% Input: 
% tri - simplices matrix {k x 4}
%
% Output:
% srf - outer boundary surfaces {no. of external surfaces x 3}
%------------------------------------------------------------------------------------------------------------------------

% create a list of all element facetts & sort
S = [tri(:,[1 2 3]); tri(:,[1 2 4]); tri(:,[2 3 4]); tri(:,[1 3 4])];
clear tri
N = sort(S,2);
clear S;
M = sortrows(N);
clear N;

i = 1;
inc = 1;

while i < size(M,1);
    ithrow = M(i,:);
    jthrow = M(i+1,:); 
    if ithrow == jthrow 
        i = i + 2;
    else
        % put in boundary node list
        srf(inc,:) = M(i,:);
        inc = inc + 1;
        i = i + 1;
    end
end
end

function [bary]=barycentre(mesh)
% compute the barycenter of all tetrahedrons
    tr=mesh.tri;
    pt=mesh.vtx;
    bary=zeros(size(tr,1),3);
    for i=1:size(tr,1)
        a=tr(i,:);
        bary(i,:)=sum(pt(a,:),1)/4;
    end
    

end