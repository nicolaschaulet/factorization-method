function [ST] = create_protocol(elec_pos,proto)
% Proto : string that contains the filename where you want to save the
% protocol
% elec_pos : electrode positioning using the format
% e1_x e1_y e1_z
%
%     ...
%
% en_x en_y en_z
    fid = fopen(elec_pos,'r');
    l=fgetl(fid);
    if length(l)<4
        nbelec = sscanf(l,'%i');
        pos = fscanf(fid,'%f %f %f',[3 nbelec]);
        pos = pos';
    else
        pos=dlmread(elec_pos);
    end
    
%     fields=fieldnames(ee);
%     pos=ee.(fields{1});
     for (i=1:size(pos,1))
        w(:,i)=1./sqrt((pos(:,1)-pos(i,1)).^2+(pos(:,2)-pos(i,2)).^2+...
            (pos(:,3)-pos(i,3)).^2);
    end
    T=tril(sparse(w));
    [ST,pred] = graphminspantree(T);
    [i,j,s]=find(ST);
    [A]=sortrows([s,i,j]);
    id=fopen(proto,'w');
    fprintf(id,'%d %d\n',[i';j']);
    fclose(id);
end