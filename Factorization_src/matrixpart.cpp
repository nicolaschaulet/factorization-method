////////////////////////////////////////////////////////////////////////
// Matrix operations
//    N. Chaulet

#include "matrixpart.hpp"

// Read a DtN matrix from ff++
mat readDtN(string & file)
{
  size_t nrows,ncolumns,nonzero;


  ifstream fin; fin.open(file.c_str());
  if (!fin)
    cout<<"Error in opening file "<<file<<endl;
 
  // read in the sparse format
  vec I(1),J(1),C(1);
  fin>>nonzero;
  I.resize(nonzero);J.resize(nonzero);C.resize(nonzero);
  for (size_t ii=0;ii<nonzero;ii++){
    fin>>I(ii); fin>>J(ii); fin>>C(ii);
  }
  fin.close();
  
  // conversion to full format
  nrows=size_t(max(I)+1);
  ncolumns=size_t(max(J)+1);
  mat A(nrows,ncolumns);A.fill(0);
  for (size_t i=0;i<nonzero;i++) {
    A(I(i),J(i))=C(i);
  }
  return A;
  
}

mat euclFourier(const int & nbeucl,const int & nbtrigo)
{
  mat trigtoeucl(nbeucl,nbtrigo);
  vec trigov1(nbeucl), trigov2(nbeucl);
  if ((nbtrigo%2)!=0){
    for (int i=0;i<nbeucl;i++) {
      trigov1(i)=cos(2*datum::pi*i/(1.*nbeucl)*((nbtrigo+1)/2));
    }
    trigov1=1./norm(trigov1,2)*trigov1;
    trigtoeucl.col(nbtrigo/2)=trigov1;
  }
  
  for (int kk=1;kk<nbtrigo/2+1;kk++){
    for (int i=0;i<nbeucl;i++) {
      trigov1(i)=cos(2*datum::pi*i/(1.*nbeucl)*kk);
      trigov2(i)=sin(2*datum::pi*i/(1.*nbeucl)*kk);
    }
    trigov1=1./norm(trigov1,2)*trigov1;
    trigov2=1./norm(trigov2,2)*trigov2;
    trigtoeucl.col(kk-1)=trigov1;
    trigtoeucl.col(kk+nbtrigo/2+nbtrigo%2-1)=trigov2;
  }
  return trigtoeucl;
}


// put noise on a matrix
void put_noise(mat &M, const double & eps)
{
  int n=M.n_rows,m=M.n_cols;
  mat Mold=M;
  mat A = randn<mat>(n,m);
  mat B=A%M;
  M=M+(eps/100)*norm(M,"fro")/norm(B,"fro")*B;
  return;
}


// build a matrix to convert an absolute potential map to a difference one
mat abs_to_dif(const int & n)
{
  mat P(n-1,n); P.fill(0);
  for (int i=0;i<n-1;i++) {
    P(i,i)=1;
    P(i,n-1)=-1;
  }
  return P;
}
