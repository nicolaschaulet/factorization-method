////////////////////////////////////////////////////////////////////////////////////////
//
//                     Loads some Armadillo features into FF++
//
////////////////////////////////////////////////////////////////////////////////////////

#include <armadillo>
#include <ff++.hpp>
#include "AFunction_ext.hpp"
#include <assert.h> 

using namespace arma;
using namespace std;


// Computes the SVD of A using the armadillo svd function and stores it in U,V and S
long ffsvd(KNM<double> *const &A,KNM<double> *const &U,KNM<double> *const &V,KN<double> *const &S)
{
  int n=A->N(),m=A->M();
  assert(n>=m); // this algo only works if m<n
  mat UU,VV,AA(n,m);
  vec SS;

  // copy the matrix A
  for (int i=0;i<n;i++) {
    for (int j=0;j<m;j++){
      AA(i,j)=(*A)(i,j);
    }
  }
  svd_econ(UU,SS,VV,AA);
 
  // Copy the result to give to FF
  V->resize(m,m);
  U->resize(n,m);
  S->resize(m);

  for (int i=0;i<n;i++) {
    for (int j=0;j<m;j++){
      if (i<m)
	(*V)(j,i)=VV(j,i);
      (*U)(i,j)=UU(i,j);
      *(S[0]+j) = SS(j);
    }
  }
  
  return 0;
}


// Computes the eigenvalues of A using the armadillo svd function and stores it in S
long ffeig(KNM<double> *const &A,KN<Complex> *const &S)
{
  int n=A->N(),m=A->M();
  assert(n==m); 
  cx_mat VV,AA(n,m);
  cx_vec SS;

  // copy the matrix A
  for (int i=0;i<n;i++) {
    for (int j=0;j<m;j++){
      AA(i,j)=(*A)(i,j);
    }
  }
  eig_gen(SS,VV,AA);
 
  // Copy the result to give to FF
  S->resize(m);
  for (int j=0;j<m;j++){
      *(S[0]+j) = SS(j);
  }
  
  return 0;
}


class Init { public:
    Init();
};

LOADINIT(Init);  //  une variable globale qui serat construite  au chargement dynamique

Init::Init(){  // le constructeur qui ajoute la fonction "splitmesh3"  a freefem++
  Global.Add("ffsvd","(",new OneOperator4_<long,KNM<double> *,KNM<double> *,KNM<double> *,KN<double> *>(ffsvd));
  Global.Add("ffeig","(",new OneOperator2_<long,KNM<double> *,KN<Complex> *>(ffeig));
}
