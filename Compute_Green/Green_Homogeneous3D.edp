/////////////////////////////////////////////////////////////////
//
//    Computes the Gradient of the Neuman Green function
//         in dimension 3 for a homogeneous domain
//
//   The strategy consists in computing the NtD map and then use it to
//   compute the Green's function -> much faster than Green3D.edp
//
////////////////////////////////////////////////////////////////

load "msh3"
load "MUMPS_FreeFem"
load "medit"
verbosity=-100;

// !!! EDIT THIS PART !!!
string filegeo="/Users/nicolas/Recherche/Codes/EIT/3DFullImpedanceModel/Data/head_31";
bool write =1; // write the solution
bool err=0; // Compute error -- Obsolete

//=================================================================
// Reading of the parameters
//=================================================================
// function to read a list of points in a file
func int readpoints(string file, real[int,int] &pp) 
{
  int nbpoints=0;
  ifstream in(file);
  in>>nbpoints;
  pp.resize(nbpoints,3);
  for (int i=0;i<nbpoints;i++){
    in>>pp(i,0); in>>pp(i,1); in>>pp(i,2);
  }
  return(1);
}

real[int,int] Pz(0,0),Elec(0,0);
mesh3 Th;
readpoints(filegeo+".grid",Pz);
readpoints(filegeo+".elec",Elec);
if (mpirank==0){
  Th=readmesh3(filegeo+"clear.mesh");
 }
broadcast(processor(0),Th);
//=================================================================
//End of reading of the parameters
//=================================================================


//=================================================================
// Finite elements spaces and formal definitions
//=================================================================
fespace Xh(Th,P1);
fespace Xh0(Th,P0);
fespace Xh2(Th,P2);
Xh2 fx,fy;
Xh solx,soly,solz,u;
Xh0 sigma,reg;
sigma=1;

//if (mpirank==0)
//  medit("T",Th,sigma);
//plot(sigma,fill=1,wait=1);
real zzx,zzy,zzz; // coordinates of the point where we compute the Green's function
macro phix(x,y,z) [1./(sigma(zzx,zzy,zzz))*((x-zzx)^2+(y-zzy)^2+(z-zzz)^2-3*(x-zzx)^2)/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.),
		   -1./sigma(zzx,zzy,zzz)*(3*(x-zzx)*(y-zzy))/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.),
		  -1./sigma(zzx,zzy,zzz)*(3*(x-zzx)*(z-zzz))/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.) ] //
macro phiy(x,y,z) [ -1./sigma(zzx,zzy,zzz)*(3*(x-zzx)*(y-zzy))/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.),
		    1./(sigma(zzx,zzy,zzz))*((x-zzx)^2+(y-zzy)^2+(z-zzz)^2-3*(y-zzy)^2)/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.),
		  -1./sigma(zzx,zzy,zzz)*(3*(y-zzy)*(z-zzz))/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.) ] //
macro phiz(x,y,z) [ -1./sigma(zzx,zzy,zzz)*(3*(x-zzx)*(z-zzz))/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.),
		    -1./sigma(zzx,zzy,zzz)*(3*(y-zzy)*(z-zzz))/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.) ,
		    1./(sigma(zzx,zzy,zzz))*((x-zzx)^2+(y-zzy)^2+(z-zzz)^2-3*(z-zzz)^2)/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(5/2.)] //
macro phi1(x,y,z) (1./sigma(zzx,zzy,zzz)*(x-zzx)/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(3/2.))// free space dipole potential (x component)
macro phi2(x,y,z) (1./sigma(zzx,zzy,zzz)*(y-zzy)/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(3/2.))// free space dipole potential (y component)
macro phi3(x,y,z) (1./sigma(zzx,zzy,zzz)*(z-zzz)/((x-zzx)^2+(y-zzy)^2+(z-zzz)^2)^(3/2.))// free space dipole potential (z component)
macro grad(f) ([dx(f),dy(f),dz(f)]) //
macro rank0() (mpirank==0)//


varf gradgrad(u,v) = int3d(Th)(sigma(x,y,z)*dx(u)*dx(v)+sigma(x,y,z)*dy(u)*dy(v)+sigma(x,y,z)*dz(u)*dz(v));
varf secmbx(u,v) = int2d(Th,0,qforder=1)(sigma(zzx,zzy,zzz)*phix(x,y,z)'*[N.x,N.y,N.z]*v*(-1.));
varf secmby(u,v) = int2d(Th,0,qforder=1)(sigma(zzx,zzy,zzz)*phiy(x,y,z)'*[N.x,N.y,N.z]*v*(-1.));
varf secmbz(u,v) = int2d(Th,0,qforder=1)(sigma(zzx,zzy,zzz)*phiz(x,y,z)'*[N.x,N.y,N.z]*v*(-1.));

varf mult(u,v) = int2d(Th,0)(1.*v); // Lagrange multiplier for 0 mean on the boundary
//=================================================================
// End of formal definitions
//=================================================================

//=================================================================
// Build the linear system 
//=================================================================
matrix A1=gradgrad(Xh,Xh);
real[int] bx1(Xh.ndof),by1(Xh.ndof),bx1vol(Xh.ndof),by1vol(Xh.ndof),bz1(Xh.ndof),bz1vol(Xh.ndof);
real[int] lag=mult(0,Xh);
matrix A=[[A1,lag],[lag',0]];
real[int] bt(Xh.ndof+1);
real[int] solt(Xh.ndof+1);
real l=0;
set(A,solver=sparsesolver);
//=================================================================
// End of building the lienar system
//=================================================================



//=================================================================
// Get all boundary nodes
//=================================================================
varf On2(u,v) = on(0,u=1);
real[int] on2=On2(0,Xh,tgv=1);
int[int] indices(on2.sum);
for(int i=0,j=0;i<Xh.ndof;++i) if(on2[i]) {indices[j] = i; ++j;}


//=================================================================
// Computes NtD map
//=================================================================
real[int,int] NtDvec(Elec.n,indices.n);

for (int i=0; i<indices.n;i++){
  bx1 = 0; bx1[indices[i]] =1;
  bt=[bx1,0];
  solt=A^-1*bt;
  solz = solx;
  [solx[],l]=solt;
  solz = solz - solx;
  for (int j=0;j<Elec.n;j++)
    NtDvec(j,i) = solx(Elec(j,0),Elec(j,1),Elec(j,2));
}

//=================================================================
// Computes Green's functions
//=================================================================
matrix NtD = NtDvec;
real[int,int] vecout(Pz.n,3*Elec.n); // to store the green function values
real[int] dvPhix(indices.n),vPhix(Elec.n),vTildePhix(Elec.n);
real[int] dvPhiy(indices.n),vPhiy(Elec.n),vTildePhiy(Elec.n);
real[int] dvPhiz(indices.n),vPhiz(Elec.n),vTildePhiz(Elec.n);

// Loop other all integration points
for (int i=0; i<Pz.n;i++){
  zzx=Pz(i,0); zzy=Pz(i,1);zzz=Pz(i,2);

  if ((rank0)&&(i%10==0))
    cout<<"Number of computed Green's functions : "<< i<<endl;
    
  real[int] tmp(Xh.ndof);
  if (mpirank ==0){
    tmp = secmbx(0,Xh);
    dvPhix = tmp(indices);
  }
  if (mpirank==1){
    tmp = secmby(0,Xh);
    dvPhiy = tmp(indices);
  }
  if (mpirank==2){
    tmp = secmbz(0,Xh);
    dvPhiz = tmp(indices);
  }
  broadcast(processor(0),dvPhix);
  broadcast(processor(1),dvPhiy);
  broadcast(processor(2),dvPhiz);


  
  // Collect the singular behaviour
  for (int i=0;i<Elec.n;i++){
    x = Elec(i,0);y=Elec(i,1); z = Elec(i,2);
    vTildePhix(i) = phi1(x,y,z);
    vTildePhiy(i) = phi2(x,y,z);
    vTildePhiz(i) = phi3(x,y,z);
  }

  // Computes the actual Neumann Green's function
  real[int] correctionPhi = NtD*dvPhix;
  vPhix = correctionPhi +vTildePhix;
  correctionPhi = NtD*dvPhiy;
  vPhiy = correctionPhi +vTildePhiy;
  correctionPhi = NtD*dvPhiz;
  vPhiz = correctionPhi +vTildePhiz;


   for (int j=0;j<Elec.n;j++){
     vecout(i,3*j)=vPhix(j);
     vecout(i,3*j+1)=vPhiy(j);
     vecout(i,3*j+2)=vPhiz(j);
  }
}


//=================================================================
// Write the results in the result file
//=================================================================
if ((mpirank==0)&&(write))
{
  ofstream out(filegeo+".grid.greenhomo");
  out<<vecout.n<<" "<<vecout.m<<endl;
  out.precision(15);
  out.scientific;
  for (int i=0;i<vecout.n;i++){
    for (int j=0;j<vecout.m;j++) {
      out<<vecout(i,j)<<" ";
    }
    if (i<(vecout.n-1))
      out<<endl;
  }
}
