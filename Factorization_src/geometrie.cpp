////////////////////////////////////////////////////////////////////////
// Geometric functions
//    N. Chaulet

#include "geometrie.hpp"

// Read a point file for electrods or mesh
void readpoints(const string &file,vector<point>  &pos)
{
  size_t dim;
  point pp;

  // Open the file
  ifstream fin; fin.open(file.c_str());
  if (!fin)
    cout<<"Error in opening file "<<file<<endl;

  // number of electrods
  fin>>dim;
  pos.resize(dim);
  
  // load the electrods
  string line;
  getline(fin,line);
  for (unsigned ii=0;ii<dim;ii++){
    getline(fin,line);
    stringstream linestream(line);
    if (!(linestream>>pp.x>>pp.y>>pp.z)){
      pp.dimension=2; pp.z=0;
      linestream>>pp.x>>pp.y;
    }
    else
      pp.dimension=3;
    pos[ii]=pp;
  }
  fin.close();  
  return ;
}


void readmesh3(const string & file,vector<point> &vtx,vector<vector<int > > &tetra)
{
  string dum;
  string line;
  ifstream fin;
  point pp;
  
   // Open the file
  fin.open(file.c_str());
  if (!fin)
    cout<<"Error in opening file "<<file<<endl;

  // test that it is the correct format
  int version=0;
  fin>>dum>>version;
  if (version!=1){
    cout<<"Wrong mesh format! "<<file<<endl;return;}

  // test right dimension of space
  int dimension;
  fin>>dum>>dimension;
  if (dimension!=3){
    cout<<"Wrong dimension of space! "<<file<<endl; return;}

  // Read Vertices
  fin>>dum>>dimension;
  vtx.resize(dimension);
  getline(fin,line);
  for (int ii=0;ii<dimension;ii++){
    getline(fin,line);
    stringstream linestream(line);
    linestream>>pp.x>>pp.y>>pp.z;
    pp.dimension=3;
    vtx[ii]=pp;
  }

   // Read Tetrahedron
  fin>>dum>>dimension;
  tetra.resize(dimension);
  getline(fin,line);
  for (int ii=0;ii<dimension;ii++){
    getline(fin,line);
    stringstream linestream(line);
    tetra[ii].resize(4);
    linestream>>tetra[ii][0]>>tetra[ii][1]>>tetra[ii][2]>>tetra[ii][3];
  }
  fin.close(); 
  return;
}

void writeVTK(const string & file,vector<point> const &vtx,vector<vector<int > > const &tetra,const vec ind )
{
  FILE * fin;
  
 // Open the file
  fin = fopen(file.c_str(),"w");
  if (!fin){
    cout<<"Error in opening file "<<file<<endl; return;}

  fprintf(fin,"# vtk DataFile Version 2.0\n" );
  fprintf(fin,"Data from factorization\n");
  fprintf(fin,"ASCII\n \n");
  fprintf(fin,"DATASET UNSTRUCTURED_GRID\n");
  fprintf(fin,"POINTS %lu FLOAT\n",vtx.size());
  for (unsigned i=0;i<vtx.size();i++){
    fprintf(fin,"%f %f %f\n",vtx[i].x,vtx[i].y,vtx[i].z);
  }

  fprintf(fin,"\nCELLS %lu %lu\n",tetra.size(),tetra.size()*(tetra[0].size()+1));
  for (unsigned i=0;i<tetra.size();i++){
    fprintf(fin,"4 %d %d %d %d\n",tetra[i][0]-1,tetra[i][1]-1,tetra[i][2]-1,tetra[i][3]-1);
  }

  fprintf(fin,"\nCELL_TYPES %lu\n",tetra.size());
  for (unsigned i=0;i<tetra.size();i++){
    fprintf(fin,"10\n");
  }
  fprintf(fin,"\nPOINT_DATA %u\nSCALARS Indicator_Function FLOAT\nLOOKUP_TABLE default\n",ind.n_rows);
  for (unsigned i=0;i<ind.n_rows;i++){
    fprintf(fin,"%f\n",ind[i]);
  }
  fclose(fin); 
  return;
}



 // Construction of the right hand side for sampling point P 
vec  computegreen(const vector<point> & electrodes, const point & P, const point & polarisation)
{
  int nbelec=electrodes.size();
  vec out(nbelec),temp(nbelec); out.fill(0);
  point ploc;

  // computation of the green function on each electrode
  for (int i=0;i<nbelec;i++) {
    ploc=electrodes[i]-P;
    if (ploc.norm()<1e-14)
      temp(i)=0;
    else 
      temp(i)=-1/(arma::datum::pi)*(ploc*polarisation)/(pow(ploc.norm(),P.dimension));
  }
  out=temp;
  return out;
}


// Computation of the Green's function for non circular geometries
void loadgreen(const string &filegeo,const int &dimension,vector<vector<point> > &G,const int & tag)
{
  string filegreen;
  if (tag==0)
    filegreen=filegeo+".green";
  if (tag==2)
    filegreen=filegeo+".greenhomo";
  
  string execff;
  int nelec,nbp;
  point PP;

  // Open the file containing the Green's function
  ifstream green; green.open(filegreen.c_str());
  if (green){
    cout<<"We read the Green's function from a file."<<endl;
  }
  else{
    cout<<"Please compute the Green's function and put it in"<<endl;
    cout<<filegreen<<endl;
    green.close();
    exit(0);
  }

  // Read the file
  green>>nbp; // number of points in the grid
  green>>nelec;
  nelec=nelec/dimension; // number of electrodes
  for (int i=0;i<nbp;i++){
    vector<point> temp;
    for (int j=0;j<nelec;j++) {
      if (dimension==2){
	green>>PP.x; green>>PP.y; PP.z=0;PP.dimension=2;
      }
      else {
	green>>PP.x; green>>PP.y; green>>PP.z;PP.dimension=3;
      }
      temp.push_back(PP);
    }
    G.push_back(temp);
  }
  green.close();
   return;
}

// Computation of the Green's function for non circular geometries (not used)
void ffcomputegreen(const string & filemesh,const string &filegeo, const string & fileelec,vector<vector<point> > &G,const int &dimension)
{
  string filegreen=filegeo+".green.debug";
  cout<<filegreen<<endl;
  ifstream green; green.open(filegreen.c_str());
  string execff;
  if (green){
    cout<<"We read the Green's function from a file."<<endl;
    green.close();
  }
  else{
    cout<<"The Green's function has to be computed."<<endl;
    cout<<"Beginning of the FF++ script."<<endl;
    if (dimension==2)
      execff="Freefem++ Compute_Green/Green2D.edp "+filemesh+" "+filegeo+" "+fileelec+" "+filegreen+" -ne";
    else
      execff="Freefem++ Compute_Green/Green3D.edp "+filemesh+" "+filegeo+" "+fileelec+" "+filegreen+" -ne";
    system(execff.c_str());
  }
  int nelec,nbp;
  point PP;
  green.open(filegreen.c_str());
  green>>nbp; // number of points in the grid
  green>>nelec;
  nelec=nelec/dimension; // number of electrodes
  cout<<nelec<<endl;
  for (int i=0;i<nbp;i++){
    vector<point> temp;
    for (int j=0;j<nelec;j++) {
      if (dimension==2){
	green>>PP.x; green>>PP.y; PP.z=0;PP.dimension=2;
      }
      else {
	green>>PP.x; green>>PP.y; green>>PP.z;PP.dimension=3;
      }
      temp.push_back(PP);
    }
    G.push_back(temp);
  }
   return;
}





// void readpoints(const string &file,vector<point2D>  &pos)
// {
//   size_t dim;
//   point2D pp;

//   // Open the file
//   ifstream fin; fin.open(file.c_str());
//   if (!fin)
//     cout<<"Error in opening file "<<file<<endl;

//   // number of points
//   fin>>dim;
//   pos.resize(dim);

//   // load the electrods
//   for (unsigned ii=0;ii<dim;ii++){
//     fin>>pp.x;
//     fin>>pp.y;
//     pos[ii]=pp;
//   }

//   fin.close();  
//   return ;
// }




// vec  computegreen(const vector<point2D> & electrodes, const point2D & P, const point2D & polarisation)
// {
//   int nbelec=electrodes.size();
//   vec out(nbelec),temp(nbelec); out.fill(0);
//   point2D ploc;

//   // computation of the green function on each electrode
//   for (int i=0;i<nbelec;i++) {
//     ploc=electrodes[i]-P;
//     if (ploc.norm()<1e-14)
//       temp(i)=0;
//     else
//       temp(i)=-1/datum::pi*(ploc*polarisation)/(pow(ploc.norm(),2));
//   }
//   out=temp;
//   return out;
// }

// void ffcomputegreen(const string & filemesh,const string &filegeo, const string & fileelec,vector<vector<point2D> > &G)
// {
//   string filegreen=filegeo+".green";
//   ifstream green; green.open(filegreen.c_str());
//   if (green){
//     cout<<"We read the Green's function from a file."<<endl;
//     green.close();
//   }
//   else{
//     cout<<"The Green's function has to be computed."<<endl;
//     cout<<"Beginning of the FF++ script."<<endl;
//     string execff="Freefem++ Compute_Green/Green2D.edp "+filemesh+" "+filegeo+" "+fileelec+" "+filegreen+" -ne";
//     system(execff.c_str());
//   }
//   int nelec,nbp;
//   point2D PP;
//   green.open(filegreen.c_str());
//   green>>nbp; // number of points in the grid
//   green>>nelec;  nelec=nelec/2; // number of electrodes
//   for (int i=0;i<nbp;i++){
//     vector<point2D> temp;
//     for (int j=0;j<nelec;j++) {
//       green>>PP.x; green>>PP.y;
//       temp.push_back(PP);      
//     }
//     G.push_back(temp);
//   }
//    return;
// }
