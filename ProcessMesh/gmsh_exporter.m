function [surfpr] = gmsh_exporter(vertices, tetrahedra,region,electrodes,  filename,center_elec)
% - vertices : vertices of the mesh
% - tetrahedra : tetrahedra of the mesh
% - electrodes is a nxm matrix and the i row contains the vertices in
%   electrode i
% - filepath = 'C:\Users\markus\Desktop\unv2mat\';
% - filename = 'bla.msh';
% - scale : scaling in the mesh

%-----------------------------------------

% Save electrode position
felec = fopen([filename '.elec'],'w');
fprintf(felec,[num2str(size(electrodes,1)) '\n']);
for i=1:size(electrodes,1)
   %fprintf(felec,[num2str(vertices(electrodes(i,1),1)) ' ' num2str(vertices(electrodes(i,1),2))...
    %   ' ' num2str(vertices(electrodes(i,1),3)) '\n']); 
   fprintf(felec,[num2str(center_elec(i,1)) ' ' num2str(center_elec(i,2))...
       ' ' num2str(center_elec(i,3)) '\n']); 
end
fclose(felec);

%-------------------------------------------------
% Creates the file for the simulation mesh
Surf=savemeshff(tetrahedra,vertices,region,electrodes,[filename '.mesh']);

%-------------------------------------------------
% Create the file for the grid


end





function [label] = findtribis(Surf,elec)
% Returns label when the triangle tri is on electrode "label" and 0 else
    numelec=size(elec,1);
    label=zeros(size(Surf,1),1);
    for (i=1:size(Surf,1))
        tri=Surf(i,:);
        for (kk=1:numelec)
            count=0;
            for (j=1:3)
                a=find(elec(kk,:)==tri(j));
                if ~isempty(a)
                    count=count+1;
                end
            end
            if (count==3)
               label(i)=kk; 
            end
        end   
    end
end

function [label] = findtri(Surf,elec)
% Returns label when the triangle tri is on electrode "label" and 0 else
    numelec=size(elec,1);
    label=0;
    for (i=1:size(Surf,1))
        tri=Surf(i,:);
        for (kk=1:numelec)
            count=0;
            for (j=1:3)
                a=find(elec(kk,:)==tri(j));
                if a
                    count=count+1;
                end
            end
            if (count==3)
               label=kk; 
            end
        end   
    end
end


function [srf] = dubs3_2(tri)
% Usage: [srf] = dubs3_2(tri);
%
% General:
% Auxiliary function that extract the boundary faces of a given 3D volume (faster than the original dubs3.m)
%
% Input: 
% tri - simplices matrix {k x 4}
%
% Output:
% srf - outer boundary surfaces {no. of external surfaces x 3}
%------------------------------------------------------------------------------------------------------------------------

% create a list of all element facetts & sort
S = [tri(:,[1 2 3]); tri(:,[1 2 4]); tri(:,[2 3 4]); tri(:,[1 3 4])];
clear tri
N = sort(S,2);
clear S;
M = sortrows(N);
clear N;

i = 1;
inc = 1;

while i < size(M,1);
    ithrow = M(i,:);
    jthrow = M(i+1,:); 
    if ithrow == jthrow 
        i = i + 2;
    else
        % put in boundary node list
        srf(inc,:) = M(i,:);
        inc = inc + 1;
        i = i + 1;
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This might become a part of the EIDORS suite
% Copyright (c) Rebecca Yerworth & Lior Horesh 2004, EIT group, Medical Physics and Bioengineering, UCL, UK
% Copying permitted under terms of GNU GPL
% See enclosed file gpl.html for details
% EIDORS 3D version 2
% MATLAB Version 6.1.0.450 (R12.1) on PCWIN
% MATLAB License Number: 111672
% Operating System: Microsoft Windows Server 2003 Standard Edition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%